# Expense Reimbursement System VI

The files in this repo were copied from [here](https://gitlab.com/christina.russ/9-21-2020-jwa/-/tree/rabinson/Project%20Submissions/project%201) for showing it on my portfolio. That is why you will not see any commit history.

## Description
The Expense Reimbursement System (ERS) manages the process of reimbursing employees for expenses incurred while on company time. All employees can log in and submit requests for reimbursement and view their past tickets and pending requests. Managers can then log in and view all reimbursement requests and past history for all employees. These managers are authorized to approve and deny requests for expense reimbursements.

## Technologies Used
- Java 8
- HTML
- CSS
- JavaScript
- Servlets
- JavaMail API
- JDBC
- SQL
- RDS
- Maven
- AJAX
- Tomcat
- Git

## Features
### List of Features
- External users can apply to be an employee. The application has to be approved by a manager for them to start as an employee. When their application is approved, they will get an email with their login information.
- Employees can login, logout, submit reimbursement requests, view pending and resolved requests, view and update their information
- Manager can do everything an employee can do. However, they can also view and approve/deny requests of employees under them, view reciepts of all reimbursement requests, view all employees and their managers, promote employees under them to managers, and add approve new users as employees

### Todo
- Make the UI more appealing
- Remove the hardcoded email and password for sending emails regarding the login information
