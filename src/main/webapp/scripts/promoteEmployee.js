(()=>{
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = ()=>{
        if(xhr.readyState === 4 & xhr.status === 200){
            let value = JSON.parse(xhr.response);

            //console.log(value);

            let body = document.getElementsByTagName("body")[0];

            let buttonList = document.createElement("div");
            buttonList.id = "buttonList";
            buttonList.innerHTML = "Select an Employee to Promote";

            let count = 0;

            for(let key in value){
                //console.log(value[key].isManager + " " + value[key].isManager === true);
                if(value[key].manager === true){
                    continue;
                }
                let button = document.createElement("button");
                button.value = value[key].employeeID;
                button.innerHTML = value[key].fName + " " + value[key].lName;
                button.addEventListener("click",()=>{promote(button.value)});
                buttonList.appendChild(button);

                count++;
            }

            if(count === 0){
                buttonList.innerHTML = "No more employees to promote!"
            }

            body.appendChild(buttonList);
            
        }
    }

    xhr.open("GET","../myapi/mGetMyEmployees",true);
    xhr.send();
})();

function promote(employeeID){
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = ()=>{
        if(xhr.readyState === 4 & xhr.status === 200){
            let value = JSON.parse(xhr.response);
            location.reload();
        }
    }

    xhr.open("GET","../myapi/promote?employeeID="+employeeID,true);
    xhr.send();
}