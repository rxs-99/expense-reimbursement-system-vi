(function(){
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = ()=>{
        if(xhr.readyState === 4 & xhr.status === 200){
            
            let info = JSON.parse(xhr.response);
            let formFields = document.getElementById("updateInfoForm").elements;

            console.log(info);
            formFields[0].value = info.fName;
            formFields[1].value = info.lName;
            formFields[2].value = info.phoneNo;
            formFields[3].value = info.email;
            formFields[4].value = new Date(info.employeeSince);
            formFields[5].value = info.managerID;



        }
    }

    xhr.open("GET","../myapi/eInfo");
    xhr.send();
})();