(()=>{
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = ()=>{
        if(xhr.readyState === 4 & xhr.status === 200){
            let value = JSON.parse(xhr.response);

            let body = document.getElementsByTagName("body")[0];

            let buttonList = document.createElement("div");
            buttonList.id = "buttonList";
            buttonList.innerHTML = "Select an Employee";

            let count = 0;

            for(let key in value){
                let button = document.createElement("button");
                button.value = value[key].employeeID;
                button.innerHTML = value[key].fName + " " + value[key].lName;
                button.addEventListener("click",()=>{toggle(button.value)});
                buttonList.appendChild(button);
                count++;
            }

            body.appendChild(buttonList);

            if(count === 0){
                buttonList.innerHTML = "You do not have any employees under you :( ... Go recruit more!"
            }
            
        }
    }

    xhr.open("GET","../myapi/mGetMyEmployees",true);
    xhr.send();
})();

function getReimbursements(id){
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
        
        if(xhr.readyState === 4 & xhr.status === 200){
            let value = JSON.parse(xhr.response);
            
            let table = document.getElementById("managerAllRequestsOfMyEmployeeTable");

            let count = 0;

            for(var key in value){
                /**/
                let row = table.insertRow(table.rows.length);

                let cell0 = row.insertCell(0);
                let cell1 = row.insertCell(1);
                let cell2 = row.insertCell(2);
                let cell3 = row.insertCell(3);
                let cell4 = row.insertCell(4);
                let cell5 = row.insertCell(5);
                let cell6 = row.insertCell(6);
                let cell7 = row.insertCell(7);
                let cell8 = row.insertCell(8);
                let cell9 = row.insertCell(9);

                cell0.innerHTML = value[key].reimbursementID;
                cell1.innerHTML = value[key].employeeID
                cell2.innerHTML = value[key].type;
                cell3.innerHTML = value[key].status;
                cell4.innerHTML = value[key].priority;

                let eventDate = new Date(value[key].eventDate);
                cell5.innerHTML = eventDate.getFullYear() + "-" + (eventDate.getMonth()+1) + "-" + eventDate.getDate();
                
                let createdDate = new Date(value[key].createdDate);
                cell6.innerHTML = createdDate.getFullYear() + "-" + (createdDate.getMonth()+1) + "-" + createdDate.getDate();

                if(value[key].fileName == null){
                    cell7.innerHTML = "No Receipt Available";
                } else {
                    let header = "data:"+value[key].fileType+";base64,";
                    let receipt = document.createElement("img");
                    receipt.src = header+value[key].content;
                    receipt.style.maxHeight = "50px";
                    receipt.style.padding = "3px";
                    cell7.appendChild(receipt);
                }

                cell8.innerHTML = value[key].amount;
                
                if(value[key].managerID == 0){
                    cell9.innerHTML = "None";
                } else {
                    cell9.innerHTML = value[key].fName + " " + value[key].lName;
                }

                count++;
            }

            if(count === 0){
                table.style.display = "none";

                let body = document.getElementsByTagName("body")[0];
                body.innerHTML = "There are no requests from this employee!";

                let gbbutton = document.createElement("button");
                gbbutton.innerHTML = "Go back";
                gbbutton.addEventListener("click",()=>{location.reload()})

                body.appendChild(gbbutton);
            }

        }
    }

    xhr.open("GET","../myapi/mGetAllRequestsByEmployee?id="+id,true);    
    xhr.send();
}

function toggle(id){
    let table = document.getElementById("managerAllRequestsOfMyEmployeeTable");
    let div = document.getElementById("buttonList");
    let button = document.getElementById("goBackButton");
    //let _body = document.getElementsByTagName("body")[0];

    if(table.hidden === true){
        div.hidden = true;
        table.hidden = false;
        button.style.display = "block";
        console.log(id);
        getReimbursements(id);
    } else {
        table.hidden = true;
        button.style.display = "none";
        cleanTable();
        div.hidden = false;
    }
}

function cleanTable(){
    let table = document.getElementById("managerAllRequestsOfMyEmployeeTable");

    let height = table.rows.length;

    for(let i = 1; i < height; i++){
        table.deleteRow(1);
    }
}