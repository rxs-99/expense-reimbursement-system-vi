(function (){
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
        
        if(xhr.readyState === 4 & xhr.status === 200){
            let value = JSON.parse(xhr.response);
            
            let table = document.getElementById("viewAllEmployeesTable");
            
            let result;
            let promises = [];

            for(var key in value){
                /**/
                let row = table.insertRow(table.rows.length);

                let cell0 = row.insertCell(0);
                let cell1 = row.insertCell(1);
                let cell2 = row.insertCell(2);
                let cell3 = row.insertCell(3);
                let cell4 = row.insertCell(4);
                let cell5 = row.insertCell(5);
                let cell6 = row.insertCell(6);

                cell0.innerHTML = value[key].employeeID;
                cell1.innerHTML = value[key].fName + " " + value[key].lName;
                cell2.innerHTML = value[key].phoneNo;
                cell3.innerHTML = value[key].email;

                let employedDate = new Date(value[key].employeeSince);
                cell4.innerHTML = employedDate.getFullYear() + "-" + (employedDate.getMonth()+1) + "-" + employedDate.getDate();

                if(value[key].isManager){
                    cell5.innerHTML = "Yes";
                } else {
                    cell5.innerHTML = "No";
                }

                cell6.innerHTML = value[key].mFName + " " + value[key].mLName;
            }

        }
    }

    xhr.open("GET","../myapi/mAllEmployees",true);    
    xhr.send();
})();