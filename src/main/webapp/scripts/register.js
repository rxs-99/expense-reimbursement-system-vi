let form = document.getElementById("registrationForm");
form.addEventListener("submit",submitForm);

function submitForm(event){
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = ()=>{
        if(xhr.readyState === 4 & xhr.status === 200){
            let response = JSON.parse(xhr.response);

            console.log(response);
            
            document.getElementById("overlay").style.display = "block";
            if(response == false){
                document.getElementById("info").innerHTML = "We had a problem submitting your information.";
                countdown_false(5);
            } else {
                document.getElementById("info").innerHTML = "Your information was successfully submitted. Once you are approved, you will recieve an email with your login info.<br>Thank you!";
                countdown_true(5);
            }
        }
    }

    let fd = new FormData(form);    

    let queryString = '';
    for(let item of fd.entries()){
        queryString += item[0] + "=" + item[1] + "&";
    }
    let finalQueryString = queryString.slice(0,-1);

    console.log(event);
    console.log(fd);
    console.log(queryString);
    console.log(finalQueryString);

    xhr.open("POST","../register?"+finalQueryString,true);
    xhr.send();

    event.preventDefault();
}

function countdown_true(remaining) {
    if(remaining <= 0)
        location.replace("../index.html");
    document.getElementById("text").innerHTML = "Redirecting in " + remaining;
    setTimeout(function(){ countdown_true(remaining - 1); }, 1000);
}

function countdown_false(remaining) {
    if(remaining <= 0)
        //document.getElementById("overlay").style.display = "none";
        location.reload();
    document.getElementById("text").innerHTML = "Try again in " + remaining;
    setTimeout(function(){ countdown_false(remaining - 1); }, 1000);
}