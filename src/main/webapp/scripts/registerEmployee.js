/**/
(function(){
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = ()=>{
        if(xhr.readyState === 4 & xhr.status === 200){
            let body = JSON.parse(xhr.response);
            let table = document.getElementById("registerEmployeeTable");

            let count = 0;
            for(var key in body){
                let row = table.insertRow(table.rows.length);

                let cell0 = row.insertCell(0);
                let cell1 = row.insertCell(1);
                let cell2 = row.insertCell(2);
                let cell3 = row.insertCell(3);
                let cell4 = row.insertCell(4);
                let cell5 = row.insertCell(5);


                cell0.innerHTML = body[key].applicationID;
                cell1.innerHTML = body[key].fName;
                cell2.innerHTML = body[key].lName;
                cell3.innerHTML = body[key].phoneNo;
                cell4.innerHTML = body[key].email;

                let selectOption = document.createElement("select");
                let option0 = document.createElement("option");
                option0.value = "none";
                option0.text = "";
                selectOption.appendChild(option0);
                let option1 = document.createElement("option");
                option1.value = "approve";
                option1.text = "Approve";
                selectOption.appendChild(option1);
                let option2 = document.createElement("option");
                option2.value = "deny";
                option2.text = "Deny";
                selectOption.appendChild(option2);

                cell5.appendChild(selectOption);

                count++;
            }

            if(count === 0){
                table.style.display = "none";
                let button = document.getElementById("submitApproveDeny");
                button.style.display = "none";
                let body = document.getElementsByTagName("body")[0];
                body.innerHTML = "There are no more applications!";
            }
        }
    }

    xhr.open("GET","../myapi/getPendingApplications");
    xhr.send();

})();
/**/

function onSubmit(){
    let table = document.getElementById("registerEmployeeTable");

    for(let i=1; i<table.rows.length; i++){
        let dropdown = (table.rows[i].cells[5]).childNodes[0];
        let option = dropdown.options[dropdown.selectedIndex].value;
        
        if(option == "approve"){
            sendApproveRequests((table.rows[i].cells[0]).childNodes[0].data);
        } else if(option == "deny"){
            sendDenyRequests((table.rows[i].cells[0]).childNodes[0].data);
        }
    }

    document.location.reload();

}

function sendApproveRequests(applicationID){
    let xhr = new XMLHttpRequest();
    xhr.open("get","../myapi/registerEmployee?applicationID="+applicationID,false);
    xhr.send();
}

function sendDenyRequests(applicationID){
    let xhr = new XMLHttpRequest();
    xhr.open("get","../myapi/rejectApplication?applicationID="+applicationID,false);
    xhr.send();
}