(()=>{
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
        if(xhr.readyState === 4 & xhr.status === 200){
            let value = JSON.parse(xhr.response);

            let body = document.getElementsByTagName("body")[0];

            let count = 0;
            for(let key in value){
                let header = "data:"+value[key].fileType+";base64,"; 
                
                let receipt = document.createElement("img");
                receipt.src=header+value[key].content;
                receipt.style.maxWidth = "200px";
                receipt.style.padding = "5px";

                body.appendChild(receipt);
                
                count++;
            }

            if(count === 0){
                body.innerHTML = "There are no more applications!";
            }
        }
    }

    xhr.open("GET","../myapi/mReciepts",true);    
    xhr.send();
})();