(function getEmployeeId(){
    let xhr = new XMLHttpRequest();
    
    xhr.onreadystatechange = () => {
        
        if(xhr.readyState === 4 & xhr.status === 200){
            let value = JSON.parse(xhr.response);
            
            let employeeName = document.getElementById("employeeName");
            employeeName.innerHTML = 'Welcome ' + value + ' !';
        }
    }

    xhr.open("GET","../myapi/getuname",true);    
    xhr.send();

})();

let iframe = document.getElementById("iframe");

function logout(){
    let xhr = new XMLHttpRequest();

    xhr.open("GET","../myapi/logout");
    xhr.send();
}

let submitReimbursementRequestButton = document.getElementById("subReimbReq");
submitReimbursementRequestButton.addEventListener("click",submitReimbursementRequest);
function submitReimbursementRequest(){    
    iframe.src="../pages/reimbursement.html";
}

let viewPendingRequestsButton = document.getElementById("viewPendingRequest");
viewPendingRequestsButton.addEventListener("click",viewPendingRequests);
function viewPendingRequests(){
    iframe.src="../pages/viewPendingRequests.html";
}

let viewResolvedRequestsButton = document.getElementById("viewResolvedRequest");
viewResolvedRequestsButton.addEventListener("click",viewResolvedRequests);
function viewResolvedRequests(){
    iframe.src="../pages/viewResolvedRequests.html"
}

let viewMyInfoButton = document.getElementById("viewMyInfo");
viewMyInfoButton.addEventListener("click",viewMyInfo);
function viewMyInfo(){
    iframe.src="../pages/viewMyInfo.html";
}

let approveDenyRequestButton = document.getElementById("approveDenyRequests");
approveDenyRequestButton.addEventListener("click",approveDenyRequests);
function approveDenyRequests(){
    iframe.src="../pages/approveDenyRequest.html";
}

let viewAllRecieptsButton = document.getElementById("viewAllReciepts");
viewAllRecieptsButton.addEventListener("click",viewAllReciepts);
function viewAllReciepts(){
    iframe.src="../pages/viewAllReceipts.html";
}

let viewAllResolvedRequestsButton = document.getElementById("viewAllResolvedRequests");
viewAllResolvedRequestsButton.addEventListener("click",viewAllResolvedRequests);
function viewAllResolvedRequests(){
    iframe.src="../pages/viewAllResolvedRequestsManager.html";
}

let viewAllEmployeesButton = document.getElementById("viewAllEmployees");
viewAllEmployeesButton.addEventListener("click",viewAllEmployees);
function viewAllEmployees(){
    iframe.src="../pages/viewAllEmployees.html";
}

let viewAllRequestsofMyEmployeeButton = document.getElementById("viewAllRequestsofMyEmployee");
viewAllRequestsofMyEmployeeButton.addEventListener("click",viewAllRequestsofMyEmployee);
function viewAllRequestsofMyEmployee(){
    iframe.src="../pages/viewAllRequestsofMyEmployee.html";
}

let promoteEmployeeButton = document.getElementById("promoteEmployee");
promoteEmployeeButton.addEventListener("click",promoteEmployee)
function promoteEmployee(){
    iframe.src="../pages/promoteEmployee.html";
}

let registerEmployeeButton = document.getElementById("registerEmployee");
registerEmployeeButton.addEventListener("click",registerEmployee)
function registerEmployee(){
    iframe.src="../pages/registerEmployee.html";
}

