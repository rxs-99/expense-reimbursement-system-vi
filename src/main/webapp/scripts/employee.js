(function getEmployeeId(){
    let xhr = new XMLHttpRequest();
    
    xhr.onreadystatechange = () => {
        
        if(xhr.readyState === 4 & xhr.status === 200){
            let value = JSON.parse(xhr.response);
            
            let employeeName = document.getElementById("employeeName");
            employeeName.innerHTML = 'Welcome ' + value + ' !';
        }
    }

    xhr.open("GET","../myapi/getuname",true);    
    xhr.send();

})();

let iframe = document.getElementById("iframe");

function logout(){
    let xhr = new XMLHttpRequest();

    xhr.open("GET","../myapi/logout");
    xhr.send();
}

let submitReimbursementRequestButton = document.getElementById("subReimbReq");
submitReimbursementRequestButton.addEventListener("click",submitReimbursementRequest);
function submitReimbursementRequest(){    
    iframe.src="reimbursement.html";
}

let viewPendingRequestsButton = document.getElementById("viewPendingRequest");
viewPendingRequestsButton.addEventListener("click",viewPendingRequests);
function viewPendingRequests(){
    iframe.src="viewPendingRequests.html";
}

let viewResolvedRequestsButton = document.getElementById("viewResolvedRequest");
viewResolvedRequestsButton.addEventListener("click",viewResolvedRequests);
function viewResolvedRequests(){
    iframe.src="viewResolvedRequests.html"
}

let viewMyInfoButton = document.getElementById("viewMyInfo");
viewMyInfoButton.addEventListener("click",viewMyInfo);
function viewMyInfo(){
    iframe.src="viewMyInfo.html";
}

