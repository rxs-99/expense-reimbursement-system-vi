(function (){
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
        
        if(xhr.readyState === 4 & xhr.status === 200){
            let value = JSON.parse(xhr.response);
            
            let table = document.getElementById("employeeResolvedRequestTable");
            
            let result;
            let promises = [];
            let count = 0;

            for(var key in value){
                /**/
                let row = table.insertRow(table.rows.length);

                let cell0 = row.insertCell(0);
                let cell1 = row.insertCell(1);
                let cell2 = row.insertCell(2);
                let cell3 = row.insertCell(3);
                let cell4 = row.insertCell(4);
                let cell5 = row.insertCell(5);
                let cell6 = row.insertCell(6);
                let cell7 = row.insertCell(7);
                let cell8 = row.insertCell(8);

                cell0.innerHTML = value[key].reimbursementID;
                cell1.innerHTML = value[key].type;
                cell2.innerHTML = value[key].status;
                cell3.innerHTML = value[key].priority;

                let eventDate = new Date(value[key].eventDate);
                cell4.innerHTML = eventDate.getFullYear() + "-" + (eventDate.getMonth()+1) + "-" + eventDate.getDate();
                
                let createdDate = new Date(value[key].createdDate);
                cell5.innerHTML = createdDate.getFullYear() + "-" + (createdDate.getMonth()+1) + "-" + createdDate.getDate();

                if(value[key].fileName == null){
                    cell6.innerHTML = "No Receipt Available";
                } else {
                    let header = "data:"+value[key].fileType+";base64,";
                    let receipt = document.createElement("img");
                    receipt.src = header+value[key].content;
                    receipt.style.maxHeight = "50px";
                    receipt.style.padding = "3px";
                    cell6.appendChild(receipt);
                } 

                cell7.innerHTML = value[key].amount;

                if(value[key].managerID == 0){
                    cell8.innerHTML = "None";
                } else {
                    cell8.innerHTML = value[key].fName + " " + value[key].lName;
                }
/**
                if(value[key].managerID != 0){
                    //let manName = getManagerName(value[key].managerID);
                    //console.log("manName");
                    //console.log(manName);
    
                    //cell8.innerHTML = manName;

                    promises.push(async(getManagerName(value[key].managerID)));
                }
/**/
                count++;
            }

            if(count === 0){
                table.style.display = "none";
                let body = document.getElementsByTagName("body")[0];
                body.innerHTML = "There are no resolved requests!";
            }

/**
            let xhr2 = new XMLHttpRequest();
            if(xhr2.readyState === 4 & xhr.status === 200){
                console.log(JSON.parse(xhr2.response));
            }
            xhr2.open("GET","../myapi/ePendingRequests",true);
            xhr2.send();
/**/
        }
    }

    xhr.open("GET","../myapi/eResolvedRequests",true);    
    xhr.send();
})();

function getManagerName(id){
    let xhr2 = new XMLHttpRequest();
    console.log("id = " + id);

    xhr2.onreadystatechange = ()=>{

        if(xhr2.status === 200){
            //console.log("response: " + xhr2.response);
            let managerName = JSON.parse(xhr2.response);
            console.log("manager Name: " + managerName);
            return managerName;
            //return xhr2.response;
        }
    }

    xhr2.open("get","../myapi/getEmployeeName?managerID="+id,true);
    xhr2.send();
}