/**/
(function(){
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = ()=>{
        if(xhr.readyState === 4 & xhr.status === 200){
            let body = JSON.parse(xhr.response);
            let table = document.getElementById("employeeResolvedRequestTable");

            let count = 0;

            for(var key in body){
                count ++;

                let row = table.insertRow(table.rows.length);

                let cell0 = row.insertCell(0);
                let cell1 = row.insertCell(1);
                let cell2 = row.insertCell(2);
                let cell3 = row.insertCell(3);
                let cell4 = row.insertCell(4);
                let cell5 = row.insertCell(5);
                let cell6 = row.insertCell(6);
                let cell7 = row.insertCell(7);
                let cell8 = row.insertCell(8);
                let cell9 = row.insertCell(9);


                cell0.innerHTML = body[key].reimbursementID;
                cell1.innerHTML = body[key].employeeID;
                cell2.innerHTML = body[key].type;
                cell3.innerHTML = body[key].status;
                cell4.innerHTML = body[key].priority;

                let eventDate = new Date(body[key].eventDate);
                cell5.innerHTML = eventDate.getFullYear() + "-" + (eventDate.getMonth()+1) + "-" + eventDate.getDate();
                
                let createdDate = new Date(body[key].createdDate);
                cell6.innerHTML = createdDate.getFullYear() + "-" + (createdDate.getMonth()+1) + "-" + createdDate.getDate();
/**
                let image = getRecieptImage(body[key].reimbursementID);
                if(image == null){
                    cell7.innerHTML = "No Receipt Available";
                } else {
                    cell7.appendChild(image);
                }
/**/
                if(body[key].fileName == null){
                    cell7.innerHTML = "No Receipt Available";
                } else {
                    let header = "data:"+body[key].fileType+";base64,";
                    let receipt = document.createElement("img");
                    receipt.src = header+body[key].content;
                    receipt.style.maxHeight = "50px";
                    receipt.style.padding = "3px";
                    cell7.appendChild(receipt);
                }

                cell8.innerHTML = body[key].amount;

                let selectOption = document.createElement("select");
                let option0 = document.createElement("option");
                option0.value = "none";
                option0.text = "";
                selectOption.appendChild(option0);
                let option1 = document.createElement("option");
                option1.value = "approve";
                option1.text = "Approve";
                selectOption.appendChild(option1);
                let option2 = document.createElement("option");
                option2.value = "deny";
                option2.text = "Deny";
                selectOption.appendChild(option2);

                cell9.appendChild(selectOption);

            }

            if(count === 0){
                table.style.display = "none";
                let button = document.getElementById("submitApproveDeny");
                button.style.display = "none";
                let body = document.getElementsByTagName("body")[0];
                body.innerHTML = "There are no more applications!";
            }

        }
    }

    xhr.open("GET","../myapi/mPendingRequests");
    xhr.send();

})();
/**/

function onSubmit(){
    let table = document.getElementById("employeeResolvedRequestTable");

    for(let i=1; i<table.rows.length; i++){
        let dropdown = (table.rows[i].cells[9]).childNodes[0];
        let option = dropdown.options[dropdown.selectedIndex].value;
        
        if(option == "approve"){
            sendApproveRequests((table.rows[i].cells[0]).childNodes[0].data , (table.rows[i].cells[4]).childNodes[0].data);
        } else if(option == "deny"){
            sendDenyRequests(table.rows[i].cells[0].childNodes[0].data , (table.rows[i].cells[4]).childNodes[0].data);
        }
    }

    document.location.reload();

    /*
    let dropdown = (table.rows[1].cells[9]).childNodes[0];
    let option = dropdown.options[dropdown.selectedIndex].value;
    console.log(option);
    */
}

function sendApproveRequests(reimbursementID,priority){
    let xhr = new XMLHttpRequest();
    xhr.open("get","../myapi/approveRequest?id="+reimbursementID+"&priority="+priority,false);
    xhr.send();
}

function sendDenyRequests(reimbursementID,priority){
    let xhr = new XMLHttpRequest();
    xhr.open("get","../myapi/denyRequest?id="+reimbursementID+"&priority="+priority,false);
    xhr.send();
}