package com.ReimbursementSystem.main;

import java.sql.Date;
import java.time.Period;
import java.util.List;

import com.ReimbursementSystem.exceptions.alreadySetUpDatabaseAuthInfoException;
import com.ReimbursementSystem.model.AuthTable;
import com.ReimbursementSystem.model.DeniedReimbursement;
import com.ReimbursementSystem.model.Employee;
import com.ReimbursementSystem.model.EmployeeApplication;
import com.ReimbursementSystem.model.Reimbursement;
import com.ReimbursementSystem.repository.AuthTableRepository;
import com.ReimbursementSystem.repository.AuthTableRepositoryImpl;
import com.ReimbursementSystem.repository.DeniedReimbursementRepository;
import com.ReimbursementSystem.repository.DeniedReimbursementRepositoryImpl;
import com.ReimbursementSystem.repository.EmployeeApplicationRepository;
import com.ReimbursementSystem.repository.EmployeeApplicationRepositoryImpl;
import com.ReimbursementSystem.repository.EmployeeRepository;
import com.ReimbursementSystem.repository.EmployeeRepositoryImpl;
import com.ReimbursementSystem.repository.ReimbursementRepository;
import com.ReimbursementSystem.repository.ReimbursementRepositoryImpl;
import com.ReimbursementSystem.service.AuthTableService;
import com.ReimbursementSystem.service.EmployeeService;
import com.ReimbursementSystem.utilities.GetAuth;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Main class
 */
public class Driver {
    public static void main(String[] args) {
        try {
            GetAuth.setProperties();
        } catch (alreadySetUpDatabaseAuthInfoException e) {
            e.printStackTrace();
        }

        /*
        Employee e1 = new Employee("a","b","c","d@aaa.aaa",2);

        EmployeeRepository er = new EmployeeRepositoryImpl();
        //er.addEmployee(e1);
        //System.out.println(er.getAllManagers().toString());

        AuthTable at = new AuthTable(3,"testUser","password");
        AuthTableRepository atr = new AuthTableRepositoryImpl();
        //atr.update(at);
        //System.out.println(atr.isDuplicateUsername("admin"));

        EmployeeApplication ea = new EmployeeApplication(3,1,"approved","i approved","aaa","bbb","ccc","e@e.e");
        EmployeeApplicationRepository ear = new EmployeeApplicationRepositoryImpl();
        //ear.update(ea);

        //List<EmployeeApplication> eal = ear.getByStatus("approveds");
        //System.out.println(eal.toString());

        Reimbursement r1 = new Reimbursement(2,3,"jjfjjdf","asdfs","eeeee",Date.valueOf("2020-10-2"),Date.valueOf("2020-10-3"),null,26.34,2);
        Reimbursement r2 = new Reimbursement(3,"test","2020-11-20",0.99);
        ReimbursementRepository rr = new ReimbursementRepositoryImpl();

        //rr.add(r2);
        //System.out.println(r2);

        //rr.update(r1);

        //System.out.println(rr.getByPriority("eeeee").toString());

        DeniedReimbursementRepository drr = new DeniedReimbursementRepositoryImpl();
        //drr.addReason(2, "testing thats why");

        //System.out.println(drr.getReason(2));
        */

        /**
        EmployeeService es = new EmployeeService();
        System.out.println(es.isManager(2));
        /**/

        //System.out.println(new EmployeeService().getAllManagers().toString());
/*
        ReimbursementRepository rr = new ReimbursementRepositoryImpl();
        List<ObjectNode> list = rr.getByStatusWithReciept("pending");

        int i = 0;
        while(i < list.size()){
            if(list.get(i).get("employeeID").asInt() != 3){
                list.remove(i);
                continue;
            }
            i++;
        }

        System.out.println(list.size());

*/

        EmployeeService es = new EmployeeService();
        System.out.print(es.getAllEmployeeByManager(2).toString());
    }
}
