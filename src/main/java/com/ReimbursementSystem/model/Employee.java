package com.ReimbursementSystem.model;

import java.sql.Date;

public class Employee {
    protected final double MAX_FUNDS = 1000.00;

    protected int employeeID;
    
    protected String fName, lName, phoneNo, email;
    
    protected boolean firstLogin;
    protected double availableFunds;
    protected Date employeeSince;
    protected int managerID;

    protected boolean isManager;

    public Employee() {

    }

    public Employee(int employeeID, String fName, String lName, String phoneNo, String email, boolean firstLogin,
            double availableFunds, Date employeeSince, int managerID, boolean isManager) {
        this.employeeID = employeeID;
        this.fName = fName;
        this.lName = lName;
        this.phoneNo = phoneNo;
        this.email = email;
        this.firstLogin = firstLogin;
        this.availableFunds = availableFunds;
        this.employeeSince = employeeSince;
        this.managerID = managerID;
        this.isManager = isManager;
    }

    public Employee(String fName, String lName, String phoneNo, String email){
        this.fName = fName;
        this.lName = lName;
        this.phoneNo = phoneNo;
        this.email = email;
        this.firstLogin = true;
        this.availableFunds = this.MAX_FUNDS;
        this.employeeSince = new Date(System.currentTimeMillis());
    }

    public Employee(String fName, String lName, String phoneNo, String email, int managerID){
        this.fName = fName;
        this.lName = lName;
        this.phoneNo = phoneNo;
        this.email = email;
        this.firstLogin = true;
        this.availableFunds = this.MAX_FUNDS;
        this.employeeSince = new Date(System.currentTimeMillis());
        this.managerID = managerID;
        this.isManager = false;
    }

    public Employee(int employeeID, String fName, String lName, String phoneNo, String email) {
        this.employeeID = employeeID;
        this.fName = fName;
        this.lName = lName;
        this.phoneNo = phoneNo;
        this.email = email;
    }

    public Employee(int employeeID, boolean isManager){
        this.employeeID = employeeID;
        this.isManager = isManager;
    }
    

    //========================================================
    public double getMAX_FUNDS() {
        return MAX_FUNDS;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int personID) {
        this.employeeID = personID;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(boolean firstLogin) {
        this.firstLogin = firstLogin;
    }

    public double getAvailableFunds() {
        return availableFunds;
    }

    public void setAvailableFunds(double availableFunds) {
        this.availableFunds = availableFunds;
    }

    public Date getEmployeeSince() {
        return employeeSince;
    }

    public void setEmployeeSince(Date employeeSince) {
        this.employeeSince = employeeSince;
    }

    public int getManagerID() {
        return managerID;
    }

    public void setManagerID(int managerID) {
        this.managerID = managerID;
    }

    public boolean isManager(){
        return this.isManager;
    }

    public void setIsManager(boolean isManager){
        this.isManager = isManager;
    }

    //========================================================

    @Override
    public String toString() {
        return "Employee [availableFunds=" + availableFunds + ", email=" + email + ", employeeSince=" + employeeSince
                + ", fName=" + fName + ", firstLogin=" + firstLogin + ", lName=" + lName + ", managerID=" + managerID
                + ", employeeID=" + employeeID + ", phoneNo=" + phoneNo + ", isManager=" + isManager + "]";
    }

}
