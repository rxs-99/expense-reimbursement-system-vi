package com.ReimbursementSystem.model;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;

public class Reimbursement {
    protected int reimbursementID, employeeID, managerID;
    protected String type, status, priority;
    protected Date eventDate, createdDate;
    protected byte[] reciept;
    protected double amount;

    public Reimbursement(){

    }
    
    public Reimbursement(int reimbursementID, int employeeID, String type, String status, String priority, Date eventDate, Date createdDate, byte[] reciept, double amount, int managerID) {
        this.reimbursementID = reimbursementID;
        this.employeeID = employeeID;
        this.type = type;
        this.status = status;
        this.priority = priority;
        this.eventDate = eventDate;
        this.createdDate = createdDate;
        this.reciept = reciept;
        this.amount = amount;
        this.managerID = managerID;
    }

    public Reimbursement(int reimbursementID, int employeeID, String type, String status, String priority, Date eventDate, Date createdDate, double amount, int managerID) {
        this.reimbursementID = reimbursementID;
        this.employeeID = employeeID;
        this.type = type;
        this.status = status;
        this.priority = priority;
        this.eventDate = eventDate;
        this.createdDate = createdDate;
        this.amount = amount;
        this.managerID = managerID;
    }

    public Reimbursement(int employeeID, String type, Date eventDate, double amount){
        this.employeeID = employeeID;
        this.type = type;
        this.eventDate = eventDate;
        this.status = "pending";
        this.createdDate = new Date(System.currentTimeMillis());

        if(this.isUrgent(this.eventDate.toLocalDate(), this.createdDate.toLocalDate())){
            this.priority = "urgent";
        } else {
            this.priority = "normal";
        }

        this.amount = amount;
        
    }

    public Reimbursement(int employeeID, String type, Date eventDate, byte[] reciept, double amount){
        this.employeeID = employeeID;
        this.type = type;
        this.eventDate = eventDate;
        this.reciept = reciept;
        this.status = "pending";
        this.createdDate = new Date(System.currentTimeMillis());

        if(this.isUrgent(this.eventDate.toLocalDate(), this.createdDate.toLocalDate())){
            this.priority = "urgent";
        } else {
            this.priority = "normal";
        }

        this.amount = amount;
    }

    public Reimbursement(int employeeID, String type, String eventDate, double amount){
        this.employeeID = employeeID;
        this.type = type;
        this.eventDate = Date.valueOf(eventDate);
        this.status = "pending";
        this.createdDate = new Date(System.currentTimeMillis());

        if(this.isUrgent(this.eventDate.toLocalDate(), this.createdDate.toLocalDate())){
            this.priority = "urgent";
        } else {
            this.priority = "normal";
        }
        
        this.amount = amount;
    }

    public Reimbursement(int employeeID, String type, String eventDate, byte[] reciept, double amount){
        this.employeeID = employeeID;
        this.type = type;
        this.eventDate = Date.valueOf(eventDate);
        this.reciept = reciept;
        this.status = "pending";
        this.createdDate = new Date(System.currentTimeMillis());

        if(this.isUrgent(this.eventDate.toLocalDate(), this.createdDate.toLocalDate())){
            this.priority = "urgent";
        } else {
            this.priority = "normal";
        }

        this.amount = amount;
    }

    public Reimbursement(int reimbursementID, String status, String priority, int managerID){
        this.reimbursementID= reimbursementID;
        this.status = status;
        this.priority = priority;
        this.managerID = managerID;
    }

    /************************************************
     ************* GETTERS AND SETTERS **************
     ************************************************/

    public int getReimbursementID() {
        return reimbursementID;
    }

    public void setReimbursementID(int reimbursementID) {
        this.reimbursementID = reimbursementID;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public byte[] getReciept() {
        return reciept;
    }

    public void setReciept(byte[] reciept) {
        this.reciept = reciept;
    }

    public double getAmount(){
        return amount;
    }

    public void setAmount(double amount){
        this.amount = amount;
    }

    public int getManagerID(){
        return managerID;
    }

    public void setManagerID(int managerID){
        this.managerID = managerID;
    }
    /************************************************
     ************************************************
     ************************************************/

     /**
      * Returns the days difference between two dates. d1 and d2 can be in any order since absolut value of the result is returned.
      * @param d1 - first date
      * @param d2 - second date
      * @return the absolute value of difference between d1 and d2
      */
    public boolean isUrgent(LocalDate d1, LocalDate d2){
        Period period = Period.between(d1, d2);
        if(Math.abs(period.getMonths()) > 0)
            return false;
        else if(Math.abs(period.getDays()) > 14)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Reimbursement [createdDate=" + createdDate + ", employeeID=" + employeeID + ", eventDate=" + eventDate
                + ", priority=" + priority + ", reimbursementID="
                + reimbursementID + ", status=" + status + ", type=" + type + ", amount=" + amount 
                + ", managerID="+ managerID + "]";
    }

    
     
}
