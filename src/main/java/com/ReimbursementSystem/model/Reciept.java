package com.ReimbursementSystem.model;

import java.io.InputStream;
import java.util.Arrays;

public class Reciept {
    private int recieptID, reimbursementID;
    private String recieptName, fileType;
    private byte[] content;
    private InputStream uploadStream;

    public Reciept(){

    }

    public Reciept(int recieptID, int reimbursementID, String recieptName, String fileType, byte[] content) {
        this.recieptID = recieptID;
        this.reimbursementID = reimbursementID;
        this.recieptName = recieptName;
        this.fileType = fileType;
        this.content = content;
    }

    public Reciept(int recieptID, int reimbursementID, String recieptName, String fileType, InputStream contentStream) {
        this.recieptID = recieptID;
        this.reimbursementID = reimbursementID;
        this.recieptName = recieptName;
        this.fileType = fileType;
        this.uploadStream = contentStream;
    }

    public Reciept(int reimbursementID, String recieptName, String fileType, InputStream uploadStream) {
        this.reimbursementID = reimbursementID;
        this.recieptName = recieptName;
        this.fileType = fileType;
        this.uploadStream = uploadStream;
    }

    public Reciept(int reimbursementID, String recieptName, String fileType, byte[] content) {
        this.reimbursementID = reimbursementID;
        this.recieptName = recieptName;
        this.fileType = fileType;
        this.content = content;
    }

    public int getRecieptID() {
        return recieptID;
    }

    public void setRecieptID(int recieptID) {
        this.recieptID = recieptID;
    }

    public int getReimbursementID() {
        return reimbursementID;
    }

    public void setReimbursementID(int reimbursementID) {
        this.reimbursementID = reimbursementID;
    }

    public String getRecieptName() {
        return recieptName;
    }

    public void setRecieptName(String recieptName) {
        this.recieptName = recieptName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public InputStream getUploadStream() {
        return uploadStream;
    }

    public void setUploadStream(InputStream uploadStream) {
        this.uploadStream = uploadStream;
    }

    @Override
    public String toString() {
        return "Reciept [content=" + Arrays.toString(content) + ", fileType=" + fileType + ", recieptID=" + recieptID
                + ", recieptName=" + recieptName + ", reimbursementID=" + reimbursementID + ", uploadStream="
                + uploadStream + "]";
    }
    
}
