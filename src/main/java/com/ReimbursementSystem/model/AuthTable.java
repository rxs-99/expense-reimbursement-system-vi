package com.ReimbursementSystem.model;

public class AuthTable {
    private int employeeID;
    private String username, password;

    public AuthTable(){

    }

    public AuthTable(int employeeID, String username, String password) {
        this.employeeID = employeeID;
        this.username = username;
        this.password = password;
    }
    
    public AuthTable(String username, String password) {
    	this.username = username;
    	this.password = password;
    }

    /************************************************
     ************* GETTERS AND SETTERS **************
     ************************************************/

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /************************************************
     ************************************************
     ************************************************/
    
    @Override
    public String toString() {
        return "AuthTable [employeeID=" + employeeID + ", password=" + password + ", username=" + username + "]";
    }

    
}
