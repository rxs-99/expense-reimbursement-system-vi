package com.ReimbursementSystem.model;

public class EmployeeApplication {
    private int applicationID, managerID;
    private String status, comment;

    private String fName, lName, phoneNo, email;

    public EmployeeApplication(){

    }
    
    public EmployeeApplication(String fName, String lName, String phoneNo, String email){
        this.fName = fName;
        this.lName = lName;
        this.phoneNo = phoneNo;
        this.email = email;
        this.status = "pending";
    }

    public EmployeeApplication(int applicationID, int managerID, String status, String comment, String fName,
            String lName, String phoneNo, String email) {
        this.applicationID = applicationID;
        this.managerID = managerID;
        this.status = status;
        this.comment = comment;
        this.fName = fName;
        this.lName = lName;
        this.phoneNo = phoneNo;
        this.email = email;
    }

    public EmployeeApplication(int applicationID, int managerID, String status, String comment){
        this.applicationID = applicationID;
        this.managerID = managerID;
        this.status = status;
        this.comment = comment;
    }

    //========================================================

    public int getApplicationID() {
        return applicationID;
    }

    public void setApplicationID(int applicationID) {
        this.applicationID = applicationID;
    }

    public int getManagerID() {
        return managerID;
    }

    public void setManagerID(int managerID) {
        this.managerID = managerID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    //========================================================

    @Override
    public String toString() {
        return "EmployeeApplication [applicationID=" + applicationID + ", comment=" + comment + ", email=" + email
                + ", fName=" + fName + ", lName=" + lName + ", managerID=" + managerID + ", phoneNo=" + phoneNo
                + ", status=" + status + "]";
    }

    


}
