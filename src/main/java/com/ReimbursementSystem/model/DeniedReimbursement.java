package com.ReimbursementSystem.model;

import java.sql.Date;

public class DeniedReimbursement extends Reimbursement {
    private String deniedReason;

    public DeniedReimbursement(){
        super();
    }

    public DeniedReimbursement(int reimbursementID, int employeeID, String type, String status, String priority, Date eventDate, Date createdDate, byte[] reciept, double amount, String deniedReason, int managerID) {
        super(reimbursementID, employeeID, type, status, priority, eventDate, createdDate, reciept, amount, managerID);
        this.deniedReason = deniedReason;
    }

    public DeniedReimbursement(int employeeID, String type, Date eventDate, double amount) {
        super(employeeID, type, eventDate, amount);
    }

    public DeniedReimbursement(int employeeID, String type, Date eventDate, byte[] reciept, double amount){
        super(employeeID, type, eventDate, reciept, amount);
    }

    /************************************************
     ************* GETTERS AND SETTERS **************
     ************************************************/

    public String getDeniedReason() {
        return deniedReason;
    }

    public void setDeniedReason(String deniedReason) {
        this.deniedReason = deniedReason;
    }

    /************************************************
     ************************************************
     ************************************************/
    
    @Override
    public String toString() {
        return "DeniedReimbursement [createdDate=" + createdDate + ", employeeID=" + employeeID + ", eventDate=" + eventDate
        + ", priority=" + priority + ", reimbursementID="
        + reimbursementID + ", status=" + status + ", type=" + type + ", amount=" + amount 
        + ", managerID=" + managerID + ", deniedReason=" + deniedReason + "]";
    }
    
}
