package com.ReimbursementSystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.ReimbursementSystem.model.Reciept;
import com.ReimbursementSystem.repository.RecieptRepository;
import com.ReimbursementSystem.utilities.DAOUtilities;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class RecieptService {
    private RecieptRepository recieptRepository;

    public RecieptService(){
        this.recieptRepository = DAOUtilities.getRecieptRepository();
    }

    public RecieptService(RecieptRepository recieptRepository){
        this.recieptRepository = recieptRepository;
    }

    public boolean add(Reciept reciept){
        return this.recieptRepository.add(reciept);
    }

    public boolean addUsingStream(Reciept reciept){
        return this.recieptRepository.addUsingStream(reciept);
    }

    public List<Reciept> getRecieptByReimbursementID(int reimbursementID){
        return this.recieptRepository.getRecieptByReimbursementID(reimbursementID);
    }

    public List<Reciept> getAll(){
        return this.recieptRepository.getAll();
    }

    public ObjectNode getRecieptByReimbursementID(HttpServletRequest request){
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode on = mapper.createObjectNode();

        List<Reciept> receiptList = this.getRecieptByReimbursementID(Integer.valueOf(request.getParameter("reimbursementID")));
        
        System.out.println(receiptList.size());
        if(receiptList.size() == 0)
            return null;

        Reciept r = receiptList.get(0);

        on.put("reimbursementID",r.getReimbursementID());
		on.put("receiptName",r.getRecieptName());
		on.put("fileType",r.getFileType());
        on.put("content",r.getContent());

        return on;
    }

    public List<ObjectNode> getAll(HttpServletRequest request){
        List<ObjectNode> reciepts = new ArrayList<ObjectNode>();
        ObjectMapper mapper = new ObjectMapper();

        List<Reciept> recieptList = this.getAll();
        for(Reciept r: recieptList){
            ObjectNode on = mapper.createObjectNode();
            
            on.put("reimbursementID",r.getReimbursementID());
			on.put("receiptName",r.getRecieptName());
			on.put("fileType",r.getFileType());
            on.put("content",r.getContent());

            reciepts.add(on);
        }

        if(reciepts.size() == 0){
            return null;
        }

        return reciepts;
    }
}
