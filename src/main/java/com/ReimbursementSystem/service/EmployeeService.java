package com.ReimbursementSystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.ReimbursementSystem.model.AuthTable;
import com.ReimbursementSystem.model.Employee;
import com.ReimbursementSystem.model.EmployeeApplication;
import com.ReimbursementSystem.repository.EmployeeRepository;
import com.ReimbursementSystem.utilities.DAOUtilities;
import com.ReimbursementSystem.utilities.Helpers;
import com.ReimbursementSystem.utilities.SendMail;
import com.ReimbursementSystem.utilities.inputValidation;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class EmployeeService {
    private EmployeeRepository employeeRepository;

    public EmployeeService(){
        this.employeeRepository = DAOUtilities.getEmployeeRepository();
    }

    public EmployeeService(EmployeeRepository er){
        this.employeeRepository = er;
    }

    public void setEmployeeRepository(EmployeeRepository er){
        this.employeeRepository = er;
    }

    public EmployeeRepository getEmployeeRepository(){
        return this.employeeRepository;
    }

    //********************************************************//

    public Employee getEmployeeByID(int id){
        return this.employeeRepository.getEmployeeByID(id);
    }

    public String getEmployeeName(int id){
        return this.getEmployeeByID(id).getfName();
    }

    public boolean addEmployee(Employee employee){
        return this.employeeRepository.addEmployee(employee);
    }

    public boolean updateEmployee(Employee employee){
        return this.employeeRepository.updateEmployee(employee);
    }

    public Employee getEmployeeByEmail(String email){
        return this.employeeRepository.getEmployeeByEmail(email);
    }

    public boolean deleteEmployee(Employee employee){
        return this.employeeRepository.deleteEmployee(employee);
    }

    public boolean deleteEmployeeByID(int id){
        return this.employeeRepository.deleteEmployeeByID(id);
    }

    public List<Employee> getAllEmployee(){
        return this.employeeRepository.getAllEmployee();
    }

    public List<Employee> getAllEmployeeByManager(int managerID){
        return this.employeeRepository.getAllEmployeeByManager(managerID);
    }

    public List<Employee> getAllManagerByJoin(){
        return this.employeeRepository.getAllManagersByJoin();
    }

    public List<Employee> getAllManagers(){
        return this.employeeRepository.getAllManagers();
    }

    public Employee getEmployeeByID(HttpServletRequest request){
        return this.getEmployeeByID((int) request.getSession().getAttribute("employeeID"));
    }

    public String getEmployeeName(HttpServletRequest request){

        System.out.println("mana = "+ request.getParameter("managerID"));

        String temp = this.getEmployeeByID(Integer.valueOf(request.getParameter("managerID"))).getfName();
        System.out.println("mana name = " + temp);
        return temp;
    }

    public boolean updateEmployee(HttpServletRequest request){
        HttpSession session = request.getSession(false);

        if(inputValidation.containSpaces(request.getParameter("fName")) || inputValidation.containSpaces(request.getParameter("lName"))){
            return false;
        } else if(!inputValidation.isOnlyAlphabets(request.getParameter("fName")) || !inputValidation.isOnlyAlphabets(request.getParameter("lName"))){
            return false;
        } else if(!inputValidation.isValidEmail(request.getParameter("email"))){
            return false;
        }

        Employee employee = new Employee((int)session.getAttribute("employeeID"),request.getParameter("fName"),request.getParameter("lName"),request.getParameter("phoneNo"),request.getParameter("email"));

        return this.updateEmployee(employee);
    }

    public boolean isManager(int employeeID){
        List<Employee> managers = this.getAllManagers();

        for(Employee e: managers){
            if(e.getEmployeeID() == employeeID)
                return true;
        }

        return false;
    }

    public boolean promoteEmployee(HttpServletRequest request){
        return this.employeeRepository.updateManagerStatus(new Employee(Integer.valueOf(request.getParameter("employeeID")),true));
    }

    public boolean registerEmployee(HttpServletRequest request){
        int managerID = (int)request.getSession().getAttribute("employeeID");
        
        EmployeeApplication ea = new EmployeeApplicationService().get(Integer.valueOf(request.getParameter("applicationID")));
        ea.setManagerID(managerID);
        ea.setStatus("approved");
        new EmployeeApplicationService().update(ea);

        Employee e = new Employee(ea.getfName(),ea.getlName(),ea.getPhoneNo(),ea.getEmail(),managerID);
        this.addEmployee(e);

        Helpers h = new Helpers();

        AuthTable auth = new AuthTable(e.getEmployeeID(),h.generateUsername(e),h.generatePassword(16));

        if(e.getEmail().contains("@gmail.com") || e.getEmail().contains("@revature.net")){
            String subject = "RSTechnologies Login Information";
            String body = "Hello " + e.getfName() + ",\n\n" 
                            + "Here is your login information for our website,\n\n"
                            + "Username: " + auth.getUsername() + "\n"+ "Password: " + auth.getPassword() + "\n\n"+ "----------------------------------------------------------\n"
                            + "Sincerely,\n"
                            + "RS Technologies Team";

            SendMail.send(SendMail.USER_NAME, SendMail.PASSWORD, e.getEmail(), subject, body);
        }

        return new AuthTableService().add(auth);
    }

    public List<ObjectNode> getAllEmployeesByJoin(){
        return this.employeeRepository.getAllEmployeesByJoin();
    }

}
