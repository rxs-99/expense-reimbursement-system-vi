package com.ReimbursementSystem.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.ReimbursementSystem.model.EmployeeApplication;
import com.ReimbursementSystem.repository.EmployeeApplicationRepository;
import com.ReimbursementSystem.utilities.DAOUtilities;
import com.ReimbursementSystem.utilities.inputValidation;

public class EmployeeApplicationService {
    
    private EmployeeApplicationRepository employeeApplicationRepository;

    public EmployeeApplicationService(){
        this.employeeApplicationRepository = DAOUtilities.getEmployeeApplicationRepository();
    }

    public EmployeeApplicationService(EmployeeApplicationRepository employeeApplicationRepository){
        this.employeeApplicationRepository = employeeApplicationRepository;
    }

    public boolean add(EmployeeApplication employeeApplication){

        if(employeeApplication.getfName() == null || employeeApplication.getlName() == null || employeeApplication.getPhoneNo() == null || employeeApplication.getEmail() == null)
            return false;
        else if(inputValidation.containSpaces(employeeApplication.getfName()) || inputValidation.containSpaces(employeeApplication.getlName()))
            return false;
        else if(!inputValidation.isOnlyAlphabets(employeeApplication.getfName()) || !inputValidation.isOnlyAlphabets(employeeApplication.getlName()))
            return false;
        else if(employeeApplication.getPhoneNo().length() < 10)
            return false;
        else if(!inputValidation.isValidEmail(employeeApplication.getEmail()))
            return false;

        return this.employeeApplicationRepository.add(employeeApplication);
    }

    public boolean update(EmployeeApplication employeeApplication){
        return this.employeeApplicationRepository.update(employeeApplication);
    }

    public EmployeeApplication get(int applicationID){
        return this.employeeApplicationRepository.get(applicationID);
    }

    public List<EmployeeApplication> getByStatus(String status){
        return this.employeeApplicationRepository.getByStatus(status);
    }

    public boolean rejectApplication(HttpServletRequest request){
        return this.update(new EmployeeApplication(Integer.valueOf(request.getParameter("applicationID")),(int) request.getSession().getAttribute("employeeID"),"denied","denied for unknown reasons!"));
    }
}
