package com.ReimbursementSystem.service;

import com.ReimbursementSystem.model.AuthTable;
import com.ReimbursementSystem.repository.AuthTableRepository;
import com.ReimbursementSystem.utilities.DAOUtilities;

public class AuthTableService {

    private AuthTableRepository authTableRepository;

    public AuthTableService(){
        this.authTableRepository = DAOUtilities.getAuthTableRepository();
    }

    public AuthTableService(AuthTableRepository authTableRepository){
        this.authTableRepository = authTableRepository;
    }

    public boolean add(AuthTable auth){
        return this.authTableRepository.add(auth);
    }
    
    public boolean update(AuthTable auth){
        return this.authTableRepository.update(auth);
    }
    
    public boolean isDuplicateUsername(String username){
        return this.authTableRepository.isDuplicateUsername(username);
    }

    public int getID(String username, String password){
        return this.authTableRepository.getID(username, password);
    }
    
    public boolean isValidLogin(String username, String password) {
        if (this.getID(username, password) == 0)
            return false;
        return true;
    }
}
