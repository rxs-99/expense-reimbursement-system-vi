package com.ReimbursementSystem.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.ReimbursementSystem.model.Reciept;
import com.ReimbursementSystem.model.Reimbursement;
import com.ReimbursementSystem.repository.ReimbursementRepository;
import com.ReimbursementSystem.utilities.DAOUtilities;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class ReimbursementService {
    private ReimbursementRepository reimbursementRepository;

    public ReimbursementService(){
        this.reimbursementRepository = DAOUtilities.getReimbursementRepository();
    }

    public ReimbursementService(ReimbursementRepository reimbursementRepository){
        this.reimbursementRepository = reimbursementRepository;
    }

    public boolean add(Reimbursement reimbursement){
        return this.reimbursementRepository.add(reimbursement);
    }

    public boolean add(HttpServletRequest request){

        /*
        try{
            // ***  NOT WORKING ... DONT KNOW WHY ***
            final String JSON = request.getReader().lines().collect(Collectors.joining());
            System.out.println(JSON);
            System.out.println("=====");
            /**/
    	
    	/**
    		String test = null;
    		try {
    			test = request.getReader().lines().collect(Collectors.joining());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		System.out.println("Test " + test);
    		/**/
    	
    		//System.out.println(request.getRequestURI());
            /**
            HttpSession session = request.getSession(false);
            int employeeID = (Integer) session.getAttribute("employeeID");
            String type = request.getParameter("type");
            String date = request.getParameter("eventDate");
            double amount = Double.parseDouble(request.getParameter("amount"));
            /**/
            //byte[] recipt = request.getParameter("reciept");
            
            //Enumeration<String> e = request.getParameterNames();
            

			//System.out.println(request.getParameter("type"));
            //System.out.println(request.getParameter("eventDate"));
            //System.out.println(request.getParameter("amount"));
            //System.out.println(request.getParameter("reciept"));
            
            //return this.add(new Reimbursement(employeeID,type,date,amount));
        /*
        } catch (IOException e){
            e.printStackTrace();
            return false;
        }
        */

        boolean isMultipartContent = ServletFileUpload.isMultipartContent(request);

        if(isMultipartContent){
            // declearing form fields
            int employeeID = (Integer) request.getSession(false).getAttribute("employeeID");
            String type = null;
            String date = null;
            double amount = 0.0;
            InputStream is = null;

            // file info
            String fieldName = null;
            String fileName = null;
            String contentType = null;

            // Create a factory for disk-based file items
            DiskFileItemFactory factory = new DiskFileItemFactory();

            // Configure a repository (to ensure a secure temp location is used)
            ServletContext servletContext = request.getServletContext();
            File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
            factory.setRepository(repository);

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);

            List<FileItem> items = null;
            // Parse the request
            try {
                items = upload.parseRequest(request);
            } catch (FileUploadException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            // Process the uploaded items
            Iterator<FileItem> iter = items.iterator();
            while (iter.hasNext()) {
                FileItem item = iter.next();

                if (item.isFormField()) {

                    try{
                        if(item.getFieldName().equals("type")){
                            type = item.getString();
                        } else if(item.getFieldName().equals("eventDate")){
                            date = item.getString();
                        } else if(item.getFieldName().equals("amount")){
                            try{
                                amount = Double.valueOf(item.getString());
                            } catch (Exception e){
                                return false;
                            }
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                        return false;
                    }

                } else {
                    fieldName = item.getFieldName();
                    fileName = item.getName();
                    contentType = item.getContentType();

                    File uploadFile = new File("R:/Revature/9-21-2020-jwa/Project Submissions/project 1/src/temp/" + fileName);                    
                    if(uploadFile.exists()){
                        uploadFile.delete();
                    }

                    try {
                        //item.write(uploadFile);
                        //new RecieptService().addUsingStream(new Reciept(7,fileName,contentType,item.getInputStream()));
                        is = item.getInputStream();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            if(type == null || date == null || type.length() ==0 || date.length() < 8){
                return false;
            } else if(amount < 0){
                return false;
            }

            // create and add reimbursement to the database, if unsuccessful return false
            Reimbursement reimbursement = new Reimbursement(employeeID,type,date,amount);
            if(!this.add(reimbursement))
                return false;

            /*System.out.println(fieldName);
            System.out.println(fileName);
            System.out.println(contentType);*/

            if(!contentType.equals("application/octet-stream")){
                // add the reciept to the database and link it to the reimbursement
                return new RecieptService().addUsingStream(new Reciept(reimbursement.getReimbursementID(),fileName,contentType,is));
            }
            return true;
        } else {
            return false;
        }        
    }


    public boolean update(Reimbursement reimbursement){
        return this.reimbursementRepository.update(reimbursement);
    }

    public List<Reimbursement> getByEmployeeID(int id){
        return this.reimbursementRepository.getByEmployeeID(id);
    }

    public List<Reimbursement> getAll(){
        return this.reimbursementRepository.getAll();
    }

    public List<Reimbursement> getByStatus(String status){
        return this.reimbursementRepository.getByStatus(status);
    }

    public List<Reimbursement> getByStatus(HttpServletRequest request){
    	System.out.println("id = " + (int)request.getSession(false).getAttribute("employeeID"));
        List<Reimbursement> reimbursements = this.getByEmployeeID((int)request.getSession(false).getAttribute("employeeID"));

        System.out.println((String)request.getAttribute("status"));
        System.out.println(reimbursements.size());
        
        int i = 0;
        while(i < reimbursements.size()){
            if(!reimbursements.get(i).getStatus().equals((String)request.getAttribute("status"))){
                reimbursements.remove(i);
                continue;
            }
            i++;
        }

        return reimbursements;
    }

    public List<ObjectNode> getByStatusWithReciept(HttpServletRequest request){
        return this.reimbursementRepository.getByStatusWithReciept((String) request.getAttribute("status"));
    }

    public List<ObjectNode> getByNotStatusWithReceipt(HttpServletRequest request){
        return this.reimbursementRepository.getByNotStatusWithReceipt((String) request.getAttribute("status"));
    }

    public List<ObjectNode> getByEmployeeIdWithReceipt(HttpServletRequest request){
        List<ObjectNode> list = null;
        
        if(request.getParameter("id") != null){
            list = this.reimbursementRepository.getByEmployeeIdWithReciept(Integer.valueOf(request.getParameter("id")));
        } else {
            list = this.reimbursementRepository.getByEmployeeIdWithReciept((int)request.getSession(false).getAttribute("employeeID"));
        }

        System.out.println(list.size());

        int i = 0;

        if(request.getAttribute("status").equals("none")){
            
        } else if (request.getAttribute("status").equals("pending")){
            while(i < list.size()){
                if(!(list.get(i).get("status").asText()).equals("pending")){
                    list.remove(i);
                    continue;
                }
                i++;
            }
        } else {
            while(i < list.size()){
                if((list.get(i).get("status").asText()).equals("pending")){
                    list.remove(i);
                    continue;
                }
                i++;
            }
        }

        System.out.println(list.size());
        return list;
    }

    public List<Reimbursement> getByNotStatus(String status){
        return this.reimbursementRepository.getByNotStatus(status);
    }

    public List<Reimbursement> getByNotStatus(HttpServletRequest request){
        List<Reimbursement> reimbursements = this.getByEmployeeID((int)request.getSession(false).getAttribute("employeeID"));

        int i = 0;
        while(i < reimbursements.size()){
            if(reimbursements.get(i).getStatus().equals((String)request.getAttribute("status"))){
                reimbursements.remove(i);
                continue;
            }
            i++;
        }

        return reimbursements;
    }
    
    public List<Reimbursement> getByPriority(String priority){
        return this.reimbursementRepository.getByPriority(priority);
    }

    public boolean approveRequest(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        Reimbursement reimbursement = new Reimbursement(Integer.valueOf(request.getParameter("id")),"approved",request.getParameter("priority"),(int) session.getAttribute("employeeID"));

        return this.update(reimbursement);
    }

    public boolean denyRequest(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        Reimbursement reimbursement = new Reimbursement(Integer.valueOf(request.getParameter("id")),"denied",request.getParameter("priority"),(int) session.getAttribute("employeeID"));

        return this.update(reimbursement);
    }
    
    public List<Reimbursement> getPendingRequestsByManagerID(int managerID){
        return this.reimbursementRepository.getPendingRequestsByManagerID(managerID);
    }

    public List<Reimbursement> getPendingRequestsByManagerID(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        return this.getPendingRequestsByManagerID((int) session.getAttribute("employeeID"));
    }

    public List<Reimbursement> getResolvedRequests(HttpServletRequest request){
        return this.getByNotStatus("pending");
    }

    public List<Reimbursement> getByEmployeeID(HttpServletRequest request){
        return this.getByEmployeeID(Integer.valueOf(request.getParameter("id")));
    }

    public List<ObjectNode> getPendingRequestsByManagerIdWithReceipt(HttpServletRequest request){
        return this.reimbursementRepository.getPendingRequestsByManagerIdWithReceipt((int) request.getSession(false).getAttribute("employeeID"));
    }
}
