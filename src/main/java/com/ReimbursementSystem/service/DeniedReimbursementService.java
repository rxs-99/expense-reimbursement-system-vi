package com.ReimbursementSystem.service;

import com.ReimbursementSystem.repository.DeniedReimbursementRepository;
import com.ReimbursementSystem.utilities.DAOUtilities;

public class DeniedReimbursementService {
    private DeniedReimbursementRepository deniedReimbursementRepository;

    public DeniedReimbursementService(){
        this.deniedReimbursementRepository = DAOUtilities.getDeniedReimbursementRepository();
    }

    public boolean addReason(int reimbursementID, String reason){
        return this.deniedReimbursementRepository.addReason(reimbursementID, reason);
    }

    public String getReason(int reimbursementID){
        return this.deniedReimbursementRepository.getReason(reimbursementID);
    }
}
