package com.ReimbursementSystem.repository;

public interface DeniedReimbursementRepository {
    public boolean addReason(int reimbursementID, String reason);
    public String getReason(int reimbursementID);
}
