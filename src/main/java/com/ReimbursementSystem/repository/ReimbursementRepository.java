package com.ReimbursementSystem.repository;

import java.util.List;

import com.ReimbursementSystem.model.Reimbursement;
import com.fasterxml.jackson.databind.node.ObjectNode;

public interface ReimbursementRepository {
    public boolean add(Reimbursement reimbursement);
    public boolean update(Reimbursement reimbursement);
    public List<Reimbursement> getByEmployeeID(int id);
    public List<Reimbursement> getAll();
    public List<Reimbursement> getByStatus(String status);
    public List<Reimbursement> getByNotStatus(String status);
    public List<Reimbursement> getByPriority(String priority);
    public List<Reimbursement> getPendingRequestsByManagerID(int managerID);
    public List<ObjectNode> getByStatusWithReciept(String status);
    public List<ObjectNode> getByEmployeeIdWithReciept(int id);
    public List<ObjectNode> getPendingRequestsByManagerIdWithReceipt(int managerID);
    public List<ObjectNode> getByNotStatusWithReceipt(String status);
}
