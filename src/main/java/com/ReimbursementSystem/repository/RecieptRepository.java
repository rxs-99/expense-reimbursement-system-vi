package com.ReimbursementSystem.repository;

import java.util.List;

import com.ReimbursementSystem.model.Reciept;

public interface RecieptRepository {
    public boolean add(Reciept reciept);
    public boolean addUsingStream(Reciept reciept);
    public List<Reciept> getRecieptByReimbursementID(int reimbursementID);
    public List<Reciept> getAll();
}
