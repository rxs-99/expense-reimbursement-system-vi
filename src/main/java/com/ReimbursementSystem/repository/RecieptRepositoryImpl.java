package com.ReimbursementSystem.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ReimbursementSystem.model.Reciept;
import com.ReimbursementSystem.utilities.DAOUtilities;

public class RecieptRepositoryImpl implements RecieptRepository {

    Connection conn = null;
	PreparedStatement stmnt = null;
	ResultSet rs = null;

    @Override
    public boolean add(Reciept reciept) {
        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "INSERT INTO reciept VALUES (default,?,?,?,?)";
			this.stmnt = this.conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);

			this.stmnt.setInt(1, reciept.getReimbursementID());
			this.stmnt.setString(2, reciept.getRecieptName());
			this.stmnt.setString(3, reciept.getFileType());
			this.stmnt.setBytes(4, reciept.getContent());

			if (this.stmnt.executeUpdate() != 0) {
				rs = stmnt.getGeneratedKeys();

				if (rs.next()) {
					reciept.setReimbursementID(rs.getInt(1));
				}

				return true;
			}
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			this.closeResources();
		}
    }

    @Override
    public boolean addUsingStream(Reciept reciept) {
        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "INSERT INTO reciept VALUES (default,?,?,?,?)";
			this.stmnt = this.conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);

			this.stmnt.setInt(1, reciept.getReimbursementID());
			this.stmnt.setString(2, reciept.getRecieptName());
			this.stmnt.setString(3, reciept.getFileType());
			this.stmnt.setBinaryStream(4, reciept.getUploadStream());

			if (this.stmnt.executeUpdate() != 0) {
				rs = stmnt.getGeneratedKeys();

				if (rs.next()) {
					reciept.setReimbursementID(rs.getInt(1));
				}

				return true;
			}
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			this.closeResources();
		}
    }

    @Override
    public List<Reciept> getRecieptByReimbursementID(int reimbursementID) {
        List<Reciept> reciepts = new ArrayList<Reciept>();

		try {
			this.conn = DAOUtilities.getConnection();
			this.stmnt = conn.prepareStatement("SELECT * FROM reciept WHERE reimbursementID = ?");

			this.stmnt.setInt(1, reimbursementID);

			rs = this.stmnt.executeQuery();

			while(rs.next()) {
                reciepts.add(new Reciept(rs.getInt("recieptID"),rs.getInt("reimbursementID"),rs.getString("recieptName"),rs.getString("fileType"),rs.getBytes("content")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
		}

		return reciepts;
    }

    @Override
    public List<Reciept> getAll() {
        List<Reciept> reciepts = new ArrayList<Reciept>();

		try {
			this.conn = DAOUtilities.getConnection();
			this.stmnt = conn.prepareStatement("SELECT * FROM reciept");

			rs = this.stmnt.executeQuery();

			while(rs.next()) {
                reciepts.add(new Reciept(rs.getInt("recieptID"),rs.getInt("reimbursementID"),rs.getString("recieptName"),rs.getString("fileType"),rs.getBytes("content")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
		}

		return reciepts;
    }

    /**
	 * Closing all resource, connection and statements
	 */
	private void closeResources() {

		try {
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			System.out.println("Could not close the result set!");
			e.printStackTrace();
		}

		try {
			if (stmnt != null)
				stmnt.close();
		} catch (SQLException e) {
			System.out.println("Could not close statement!");
			e.printStackTrace();
		}

		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e) {
			System.out.println("Could not close the connection!");
			e.printStackTrace();
		}
	}
    
}
