package com.ReimbursementSystem.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ReimbursementSystem.utilities.DAOUtilities;

public class DeniedReimbursementRepositoryImpl implements DeniedReimbursementRepository {

    private Connection conn = null;
    private PreparedStatement stmnt = null;
    private ResultSet rs = null;
    
    @Override
    public boolean addReason(int reimbursementID, String reason) {
        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "INSERT INTO deniedReimbursement VALUES (?,?)";
			this.stmnt = this.conn.prepareStatement(sql);
			
			this.stmnt.setInt(1, reimbursementID);
			this.stmnt.setString(2, reason);
			
			if(this.stmnt.executeUpdate() != 0) {
                return true;
            }
			return false;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			this.closeResources();
		}
    }

    @Override
    public String getReason(int reimbursementID) {
        String deniedReason = null;
        
        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "SELECT deniedReason FROM deniedReimbursement WHERE reimbID=?";
			this.stmnt = this.conn.prepareStatement(sql);
			
			this.stmnt.setInt(1, reimbursementID);
			
            this.rs = this.stmnt.executeQuery();
            
            if(rs.next())
                deniedReason = rs.getString("deniedReason");
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
        }
        
        return deniedReason;
    }

    /**
	 * Closing all resource, connection and statements
	 */
	private void closeResources() {
		
		try {
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			System.out.println("Could not close the result set!");
			e.printStackTrace();
		}

		
		try {
			if (stmnt != null)
				stmnt.close();
		} catch (SQLException e) {
			System.out.println("Could not close statement!");
			e.printStackTrace();
		}

		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e) {
			System.out.println("Could not close the connection!");
			e.printStackTrace();
		}		
	}
    
}
