package com.ReimbursementSystem.repository;

import java.util.List;

import com.ReimbursementSystem.model.EmployeeApplication;

public interface EmployeeApplicationRepository {
    public boolean add(EmployeeApplication employeeApplication);
    public boolean update(EmployeeApplication employeeApplication);
    public EmployeeApplication get(int applicationID);
    public List<EmployeeApplication> getByStatus(String status);
}
