package com.ReimbursementSystem.repository;

import com.ReimbursementSystem.model.AuthTable;

public interface AuthTableRepository {
    public boolean add(AuthTable auth);
    public boolean update(AuthTable auth);
    public int getID(String username, String password);
    public boolean isDuplicateUsername(String username);
}
