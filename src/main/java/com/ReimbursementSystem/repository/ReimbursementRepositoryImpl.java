package com.ReimbursementSystem.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ReimbursementSystem.model.Reimbursement;
import com.ReimbursementSystem.utilities.DAOUtilities;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ReimbursementRepositoryImpl implements ReimbursementRepository {
    private Connection conn = null;
	private PreparedStatement stmnt = null;
    private ResultSet rs = null;
    
    @Override
    public boolean add(Reimbursement reimbursement) {
        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "INSERT INTO reimbursement VALUES (default,?,?,?,?,?,?,?,?,null)";
			this.stmnt = this.conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);

			this.stmnt.setInt(1, reimbursement.getEmployeeID());
			this.stmnt.setString(2, reimbursement.getType());
			this.stmnt.setBytes(3, reimbursement.getReciept());
			this.stmnt.setString(4, reimbursement.getStatus());
			this.stmnt.setDate(5, reimbursement.getCreatedDate());
			this.stmnt.setString(6, reimbursement.getPriority());
            this.stmnt.setDate(7, reimbursement.getEventDate());
            this.stmnt.setDouble(8, reimbursement.getAmount());

			if (this.stmnt.executeUpdate() != 0) {
				rs = stmnt.getGeneratedKeys();

				if (rs.next()) {
					reimbursement.setReimbursementID(rs.getInt(1));
				}

				return true;
			}
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			this.closeResources();
		}
    }

    @Override
    public boolean update(Reimbursement reimbursement) {
        try {
			this.conn = DAOUtilities.getConnection();
            String sql = "";
            if(reimbursement.getManagerID() == 0) {
                sql = "UPDATE reimbursement SET status=?, priority=? WHERE reimbID=?";
                this.stmnt = this.conn.prepareStatement(sql);

			    this.stmnt.setString(1, reimbursement.getStatus());
                this.stmnt.setString(2, reimbursement.getPriority());
                this.stmnt.setInt(3, reimbursement.getReimbursementID());
            }
            else {
                sql = "UPDATE reimbursement SET status=?, priority=?, managerID=? WHERE reimbID=?";
                this.stmnt = this.conn.prepareStatement(sql);

			    this.stmnt.setString(1, reimbursement.getStatus());
                this.stmnt.setString(2, reimbursement.getPriority());
                this.stmnt.setInt(3, reimbursement.getManagerID());
                this.stmnt.setInt(4, reimbursement.getReimbursementID());
            }

			if (this.stmnt.executeUpdate() != 0 ) {
				return true;
			}
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			this.closeResources();
		}
    }

    @Override
    public List<Reimbursement> getByEmployeeID(int id) {
        List<Reimbursement> reimbursements = new ArrayList<Reimbursement>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "SELECT * FROM reimbursement WHERE employeeID=?";
			this.stmnt = this.conn.prepareStatement(sql);

            this.stmnt.setInt(1, id);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {
                reimbursements.add(new Reimbursement(rs.getInt("reimbID"),rs.getInt("employeeID"),rs.getString("type"),rs.getString("status"),rs.getString("priority"),rs.getDate("eventDate"),rs.getDate("createdOn"),rs.getBytes("reciept"),rs.getDouble("amount"),rs.getInt("managerID")));
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
        }
        
        return reimbursements;
    }

    @Override
    public List<Reimbursement> getAll() {        
        List<Reimbursement> reimbursements = new ArrayList<Reimbursement>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "SELECT * FROM reimbursement";
			this.stmnt = this.conn.prepareStatement(sql);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {
                reimbursements.add(new Reimbursement(rs.getInt("reimbID"),rs.getInt("employeeID"),rs.getString("type"),rs.getString("status"),rs.getString("priority"),rs.getDate("eventDate"),rs.getDate("createdOn"),rs.getBytes("reciept"),rs.getDouble("amount"),rs.getInt("managerID")));
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
        }
        
        return reimbursements;
    }

    @Override
    public List<Reimbursement> getByStatus(String status) {
        List<Reimbursement> reimbursements = new ArrayList<Reimbursement>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "SELECT * FROM reimbursement WHERE status=?";
			this.stmnt = this.conn.prepareStatement(sql);

            this.stmnt.setString(1, status);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {
                reimbursements.add(new Reimbursement(rs.getInt("reimbID"),rs.getInt("employeeID"),rs.getString("type"),rs.getString("status"),rs.getString("priority"),rs.getDate("eventDate"),rs.getDate("createdOn"),rs.getBytes("reciept"),rs.getDouble("amount"),rs.getInt("managerID")));
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
        }
        
        return reimbursements;
	}
	
	@Override
	public List<Reimbursement> getByNotStatus(String status){
		List<Reimbursement> reimbursements = new ArrayList<Reimbursement>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "SELECT * FROM reimbursement WHERE NOT status=?";
			this.stmnt = this.conn.prepareStatement(sql);

            this.stmnt.setString(1, status);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {
                reimbursements.add(new Reimbursement(rs.getInt("reimbID"),rs.getInt("employeeID"),rs.getString("type"),rs.getString("status"),rs.getString("priority"),rs.getDate("eventDate"),rs.getDate("createdOn"),rs.getBytes("reciept"),rs.getDouble("amount"),rs.getInt("managerID")));
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
        }
        
        return reimbursements;
	}

    @Override
    public List<Reimbursement> getByPriority(String priority) {
        List<Reimbursement> reimbursements = new ArrayList<Reimbursement>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "SELECT * FROM reimbursement WHERE priority=?";
			this.stmnt = this.conn.prepareStatement(sql);

            this.stmnt.setString(1, priority);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {
                reimbursements.add(new Reimbursement(rs.getInt("reimbID"),rs.getInt("employeeID"),rs.getString("type"),rs.getString("status"),rs.getString("priority"),rs.getDate("eventDate"),rs.getDate("createdOn"),rs.getBytes("reciept"),rs.getDouble("amount"),rs.getInt("managerID")));
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
        }
        
        return reimbursements;
	}
	
	@Override
	public List<Reimbursement> getPendingRequestsByManagerID(int managerID) {
		List<Reimbursement> reimbursements = new ArrayList<Reimbursement>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "SELECT r.* FROM reimbursement r INNER JOIN employee e ON r.employeeid=e.employeeid WHERE e.managerid=? AND r.status='pending'";
			this.stmnt = this.conn.prepareStatement(sql);

            this.stmnt.setInt(1, managerID);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {
                reimbursements.add(new Reimbursement(rs.getInt("reimbID"),rs.getInt("employeeID"),rs.getString("type"),rs.getString("status"),rs.getString("priority"),rs.getDate("eventDate"),rs.getDate("createdOn"),rs.getBytes("reciept"),rs.getDouble("amount"),rs.getInt("managerID")));
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
        }
        
        return reimbursements;
	}

	@Override
	public List<ObjectNode> getPendingRequestsByManagerIdWithReceipt(int managerID){
		ObjectMapper mapper = new ObjectMapper();
		List<ObjectNode> list = new ArrayList<ObjectNode>();

		try {
			this.conn = DAOUtilities.getConnection();
			String sql = "select a.*, r2.recieptname, r2.filetype, r2.content from (select a.*, e2.managerid as mngId from (select r.*,e.fname, e.lname from reimbursement r left join employee e on r.managerid = e.employeeid) as a inner join employee e2 on a.employeeid = e2.employeeid) a full join reciept r2 on a.reimbid = r2.reimbursementid  where status = 'pending' and mngid=?";
			this.stmnt = this.conn.prepareStatement(sql);

            this.stmnt.setInt(1, managerID);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {

				ObjectNode on = mapper.createObjectNode();
				on.put("reimbursementID",rs.getInt("reimbID"));
				on.put("employeeID",rs.getInt("employeeid"));
				on.put("type",rs.getString("type"));
				on.put("status",rs.getString("status"));
				on.put("priority",rs.getString("priority"));
				on.put("eventDate",rs.getDate("eventDate").getTime());
				on.put("createdDate",rs.getDate("createdOn").getTime());
				on.put("amount",rs.getDouble("amount"));
				on.put("managerID",rs.getInt("managerID"));
				on.put("fName",rs.getString("fname"));
				on.put("lName",rs.getString("lname"));
				on.put("fileType",rs.getString("filetype"));
				on.put("fileName",rs.getString("recieptname"));
				on.put("content",rs.getBytes("content"));

				list.add(on);
				
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
		}

		return list;
	}

	@Override
	public List<ObjectNode> getByStatusWithReciept(String status){
		ObjectMapper mapper = new ObjectMapper();
		List<ObjectNode> list = new ArrayList<ObjectNode>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "select a.*, r2.recieptname, r2.filetype, r2.content from (select r.*,e.fname, e.lname from reimbursement r left join employee e on r.managerid = e.employeeid) a full join reciept r2 on a.reimbid = r2.reimbursementid  where a.status = ?";
			this.stmnt = this.conn.prepareStatement(sql);

            this.stmnt.setString(1, status);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {

				ObjectNode on = mapper.createObjectNode();
				on.put("reimbursementID",rs.getInt("reimbID"));
				on.put("employeeID",rs.getInt("employeeid"));
				on.put("type",rs.getString("type"));
				on.put("status",rs.getString("status"));
				on.put("priority",rs.getString("priority"));
				on.put("eventDate",rs.getDate("eventDate").getTime());
				on.put("createdDate",rs.getDate("createdOn").getTime());
				on.put("amount",rs.getDouble("amount"));
				on.put("managerID",rs.getInt("managerID"));
				on.put("fName",rs.getString("fname"));
				on.put("lName",rs.getString("lname"));
				on.put("fileType",rs.getString("filetype"));
				on.put("fileName",rs.getString("recieptname"));
				on.put("content",rs.getBytes("content"));

				list.add(on);
				
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
		}
				
		return list;
	}

	@Override
	public List<ObjectNode> getByNotStatusWithReceipt(String status){
		ObjectMapper mapper = new ObjectMapper();
		List<ObjectNode> list = new ArrayList<ObjectNode>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "select a.*, r2.recieptname, r2.filetype, r2.content from (select r.*,e.fname, e.lname from reimbursement r left join employee e on r.managerid = e.employeeid) a full join reciept r2 on a.reimbid = r2.reimbursementid  where not a.status = ?";
			this.stmnt = this.conn.prepareStatement(sql);

            this.stmnt.setString(1, status);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {

				ObjectNode on = mapper.createObjectNode();
				on.put("reimbursementID",rs.getInt("reimbID"));
				on.put("employeeID",rs.getInt("employeeid"));
				on.put("type",rs.getString("type"));
				on.put("status",rs.getString("status"));
				on.put("priority",rs.getString("priority"));
				on.put("eventDate",rs.getDate("eventDate").getTime());
				on.put("createdDate",rs.getDate("createdOn").getTime());
				on.put("amount",rs.getDouble("amount"));
				on.put("managerID",rs.getInt("managerID"));
				on.put("fName",rs.getString("fname"));
				on.put("lName",rs.getString("lname"));
				on.put("fileType",rs.getString("filetype"));
				on.put("fileName",rs.getString("recieptname"));
				on.put("content",rs.getBytes("content"));

				list.add(on);
				
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
		}
				
		return list;
	}

	@Override
	public List<ObjectNode> getByEmployeeIdWithReciept(int id){
		ObjectMapper mapper = new ObjectMapper();
		List<ObjectNode> list = new ArrayList<ObjectNode>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "select a.*, r2.recieptname, r2.filetype, r2.content from (select r.*,e.fname, e.lname from reimbursement r left join employee e on r.managerid = e.employeeid) a full join reciept r2 on a.reimbid = r2.reimbursementid  where a.employeeID = ?";
			this.stmnt = this.conn.prepareStatement(sql);

            this.stmnt.setInt(1, id);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {

				ObjectNode on = mapper.createObjectNode();
				on.put("reimbursementID",rs.getInt("reimbID"));
				on.put("employeeID",rs.getInt("employeeid"));
				on.put("type",rs.getString("type"));
				on.put("status",rs.getString("status"));
				on.put("priority",rs.getString("priority"));
				on.put("eventDate",rs.getDate("eventDate").getTime());
				on.put("createdDate",rs.getDate("createdOn").getTime());
				on.put("amount",rs.getDouble("amount"));
				on.put("managerID",rs.getInt("managerID"));
				on.put("fName",rs.getString("fname"));
				on.put("lName",rs.getString("lname"));
				on.put("fileType",rs.getString("filetype"));
				on.put("fileName",rs.getString("recieptname"));
				on.put("content",rs.getBytes("content"));

				list.add(on);
				
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
		}
				
		return list;
	}
    
    /**
	 * Closing all resource, connection and statements
	 */
	private void closeResources() {

		try {
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			System.out.println("Could not close the result set!");
			e.printStackTrace();
		}

		try {
			if (stmnt != null)
				stmnt.close();
		} catch (SQLException e) {
			System.out.println("Could not close statement!");
			e.printStackTrace();
		}

		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e) {
			System.out.println("Could not close the connection!");
			e.printStackTrace();
		}
	}
}
