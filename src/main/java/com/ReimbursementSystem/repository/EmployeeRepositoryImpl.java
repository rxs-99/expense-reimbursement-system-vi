package com.ReimbursementSystem.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ReimbursementSystem.model.Employee;
import com.ReimbursementSystem.utilities.DAOUtilities;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class EmployeeRepositoryImpl implements EmployeeRepository {

	Connection conn = null;
	PreparedStatement stmnt = null;
	ResultSet rs = null;

	@Override
	public Employee getEmployeeByID(int id) {
		Employee employee = new Employee();

		try {
			this.conn = DAOUtilities.getConnection();
			this.stmnt = conn.prepareStatement("SELECT * FROM employee WHERE employeeID = ?");

			this.stmnt.setInt(1, id);

			rs = this.stmnt.executeQuery();

			if (rs.next()) {
				employee.setEmployeeID(rs.getInt("employeeID"));
				employee.setfName(rs.getString("fName"));
				employee.setlName(rs.getString("lName"));
				employee.setPhoneNo(rs.getString("phoneNumber"));
				employee.setEmail(rs.getString("email"));
				employee.setFirstLogin(rs.getBoolean("firstLogin"));
				employee.setAvailableFunds(rs.getDouble("availableFunds"));
				employee.setEmployeeSince(rs.getDate("employeeSince"));
				employee.setManagerID(rs.getInt("managerID"));
				employee.setIsManager(rs.getBoolean("isManager"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
		}

		return employee;
	}

	@Override
	public boolean addEmployee(Employee employee) {
		try {
			this.conn = DAOUtilities.getConnection();
			String sql = "INSERT INTO employee VALUES (default,?,?,?,?,?,?,?,?,?)";
			this.stmnt = this.conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);

			this.stmnt.setString(1, employee.getfName());
			this.stmnt.setString(2, employee.getlName());
			this.stmnt.setString(3, employee.getPhoneNo());
			this.stmnt.setString(4, employee.getEmail());
			this.stmnt.setBoolean(5, employee.isFirstLogin());
			this.stmnt.setDouble(6, employee.getAvailableFunds());
			this.stmnt.setDate(7, employee.getEmployeeSince());
			this.stmnt.setInt(8, employee.getManagerID());
			this.stmnt.setBoolean(9, employee.isManager());

			if (this.stmnt.executeUpdate() != 0) {
				rs = stmnt.getGeneratedKeys();

				if (rs.next()) {
					employee.setEmployeeID(rs.getInt(1));
				}

				return true;
			}
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			this.closeResources();
		}

	}

	@Override
	public boolean updateEmployee(Employee employee) {
		try {
			this.conn = DAOUtilities.getConnection();
			String sql = "UPDATE employee SET fName=?, lName=?, phoneNumber=?, email=? WHERE employeeID=?";
			this.stmnt = this.conn.prepareStatement(sql);

			this.stmnt.setString(1, employee.getfName());
			this.stmnt.setString(2, employee.getlName());
			this.stmnt.setString(3, employee.getPhoneNo());
			this.stmnt.setString(4, employee.getEmail());
			this.stmnt.setInt(5, employee.getEmployeeID());

			if (this.stmnt.executeUpdate() != 0) {
				return true;
			}
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			this.closeResources();
		}
	}

	@Override
	public Employee getEmployeeByEmail(String email) {
		Employee employee = new Employee();

		try {
			this.conn = DAOUtilities.getConnection();
			this.stmnt = conn.prepareStatement("SELECT * FROM employee WHERE email = ?");

			this.stmnt.setString(1, email);

			rs = this.stmnt.executeQuery();

			if (rs.next()) {
				employee.setEmployeeID(rs.getInt("employeeID"));
				employee.setfName(rs.getString("fName"));
				employee.setlName(rs.getString("lName"));
				employee.setPhoneNo(rs.getString("phoneNumber"));
				employee.setEmail(rs.getString("email"));
				employee.setFirstLogin(rs.getBoolean("firstLogin"));
				employee.setAvailableFunds(rs.getDouble("availableFunds"));
				employee.setEmployeeSince(rs.getDate("employeeSince"));
				employee.setManagerID(rs.getInt("managerID"));
				employee.setIsManager(rs.getBoolean("isManager"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
		}

		return employee;

	}

	@Override
	public boolean deleteEmployee(Employee employee) {
		// TODO
		return false;
	}

	@Override
	public boolean deleteEmployeeByID(int id) {
		// TODO
		return false;
	}

	@Override
	public List<Employee> getAllEmployee() {
		List<Employee> employees = new ArrayList<Employee>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "SELECT * FROM employee";
			this.stmnt = this.conn.prepareStatement(sql);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {
                employees.add(new Employee(rs.getInt("employeeID"),rs.getString("fName"),rs.getString("lName"),rs.getString("phoneNumber"),rs.getString("email"),rs.getBoolean("firstLogin"),rs.getDouble("availableFunds"),rs.getDate("employeeSince"),rs.getInt("managerID"),rs.getBoolean("isManager")));
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
        }
        
        return employees;
	}

	@Override
	public List<Employee> getAllEmployeeByManager(int managerID) {
		List<Employee> employees = new ArrayList<Employee>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "SELECT * FROM employee WHERE managerID=?";
			this.stmnt = this.conn.prepareStatement(sql);

			this.stmnt.setInt(1, managerID);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {
                employees.add(new Employee(rs.getInt("employeeID"),rs.getString("fName"),rs.getString("lName"),rs.getString("phoneNumber"),rs.getString("email"),rs.getBoolean("firstLogin"),rs.getDouble("availableFunds"),rs.getDate("employeeSince"),rs.getInt("managerID"),rs.getBoolean("isManager")));
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
        }
        
        return employees;
	}

	@Override
	public List<Employee> getAllManagersByJoin() {
		List<Employee> employees = new ArrayList<Employee>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "SELECT e.* from employee as e "
							+ "INNER JOIN "
							+ "(SELECT managerid FROM employee "
							+ "WHERE managerid IS NOT NULL "
							+ "GROUP BY managerid) as j "
							+ "on e.employeeid = j.managerid";
			this.stmnt = this.conn.prepareStatement(sql);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {
                employees.add(new Employee(rs.getInt("employeeID"),rs.getString("fName"),rs.getString("lName"),rs.getString("phoneNumber"),rs.getString("email"),rs.getBoolean("firstLogin"),rs.getDouble("availableFunds"),rs.getDate("employeeSince"),rs.getInt("managerID"),rs.getBoolean("isManager")));
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
        }
        
        return employees;
	}

	@Override
	public List<Employee> getAllManagers() {
		List<Employee> employees = new ArrayList<Employee>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "SELECT * FROM employee WHERE isManager=true";
			this.stmnt = this.conn.prepareStatement(sql);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {
                employees.add(new Employee(rs.getInt("employeeID"),rs.getString("fName"),rs.getString("lName"),rs.getString("phoneNumber"),rs.getString("email"),rs.getBoolean("firstLogin"),rs.getDouble("availableFunds"),rs.getDate("employeeSince"),rs.getInt("managerID"),rs.getBoolean("isManager")));
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
        }
        
        return employees;
	}

	@Override
	public boolean updateManagerStatus(Employee employee){
		try {
			this.conn = DAOUtilities.getConnection();
			String sql = "UPDATE employee SET isManager=? WHERE employeeID=?";
			this.stmnt = this.conn.prepareStatement(sql);

			this.stmnt.setBoolean(1, employee.isManager());
			this.stmnt.setInt(2, employee.getEmployeeID());

			if (this.stmnt.executeUpdate() != 0) {
				return true;
			}
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			this.closeResources();
		}
	}

	@Override
	public List<ObjectNode> getAllEmployeesByJoin(){
		ObjectMapper mapper = new ObjectMapper();
		List<ObjectNode> list = new ArrayList<ObjectNode>();

        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "select e.*, e2.fname as mfName, e2.lname as mlName from employee e left join employee e2 on e.managerid = e2.employeeid ;";
			this.stmnt = this.conn.prepareStatement(sql);

            this.rs = this.stmnt.executeQuery();

			while(rs.next()) {
				ObjectNode on = mapper.createObjectNode();
				on.put("employeeID",rs.getInt("employeeID"));
				on.put("fName",rs.getString("fName"));
				on.put("lName",rs.getString("lName"));
				on.put("phoneNo",rs.getString("phoneNumber"));
				on.put("email",rs.getString("email"));
				on.put("employeeSince",rs.getDate("employeeSince").getTime());
				on.put("isManager",rs.getBoolean("isManager"));
				on.put("mFName",rs.getString("mFName"));
				on.put("mLName",rs.getString("mLName"));

                list.add(on);
            }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
		}
		
		return list;
	}
	/**
	 * Closing all resource, connection and statements
	 */
	private void closeResources() {

		try {
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			System.out.println("Could not close the result set!");
			e.printStackTrace();
		}

		try {
			if (stmnt != null)
				stmnt.close();
		} catch (SQLException e) {
			System.out.println("Could not close statement!");
			e.printStackTrace();
		}

		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e) {
			System.out.println("Could not close the connection!");
			e.printStackTrace();
		}
	}

}
