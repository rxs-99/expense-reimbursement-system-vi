package com.ReimbursementSystem.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ReimbursementSystem.model.AuthTable;
import com.ReimbursementSystem.utilities.DAOUtilities;

public class AuthTableRepositoryImpl implements AuthTableRepository {

    private Connection conn = null;
    private PreparedStatement stmnt = null;
    private ResultSet rs = null;

    @Override
    public boolean add(AuthTable auth) {
        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "INSERT INTO authTable VALUES (?,?,?)";
			this.stmnt = this.conn.prepareStatement(sql);
			
			this.stmnt.setInt(1, auth.getEmployeeID());
			this.stmnt.setString(2, auth.getUsername());
			this.stmnt.setString(3, auth.getPassword());
			
			if(this.stmnt.executeUpdate() != 0) {
                return true;
            }
			return false;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			this.closeResources();
		}
    }

    @Override
    public boolean update(AuthTable auth) {
        try {
			this.conn = DAOUtilities.getConnection();
			String sql = "UPDATE authTable SET password=? WHERE employeeID=?";
			this.stmnt = this.conn.prepareStatement(sql);
			
			this.stmnt.setString(1, auth.getPassword());
			this.stmnt.setInt(2, auth.getEmployeeID());
			
			if(this.stmnt.executeUpdate() != 0) {
                return true;
            }
			return false;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			this.closeResources();
		}
	}
	
	@Override
	public boolean isDuplicateUsername(String username){
		try {
			this.conn = DAOUtilities.getConnection();
			String sql = "SELECT * FROM authTable WHERE username = ?";
			this.stmnt = conn.prepareStatement(sql);
			
			this.stmnt.setString(1, username);
			
			rs = this.stmnt.executeQuery();
			
			if(rs.next())
				return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return true;
		} finally {
			this.closeResources();
		}
		
		return false;

	}
	
	@Override
	public int getID(String username, String password) {
		int id = 0;
		
		try {
			this.conn = DAOUtilities.getConnection();
			String sql = "SELECT employeeID FROM authTable WHERE username = ? and password = ?";
			this.stmnt = conn.prepareStatement(sql);
			
			this.stmnt.setString(1, username);
			this.stmnt.setString(2, password);
			
			this.rs = stmnt.executeQuery();
			
			if(this.rs.next())
				id = this.rs.getInt("employeeID");
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeResources();
		}
		
		return id;
	}

    /**
	 * Closing all resource, connection and statements
	 */
	private void closeResources() {
		
		try {
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			System.out.println("Could not close the result set!");
			e.printStackTrace();
		}

		
		try {
			if (stmnt != null)
				stmnt.close();
		} catch (SQLException e) {
			System.out.println("Could not close statement!");
			e.printStackTrace();
		}

		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e) {
			System.out.println("Could not close the connection!");
			e.printStackTrace();
		}		
	}		
}

