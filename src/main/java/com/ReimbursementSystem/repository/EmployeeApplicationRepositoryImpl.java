package com.ReimbursementSystem.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ReimbursementSystem.model.EmployeeApplication;
import com.ReimbursementSystem.utilities.DAOUtilities;

public class EmployeeApplicationRepositoryImpl implements EmployeeApplicationRepository {

    private Connection conn = null;
    private PreparedStatement stmnt = null;
    private ResultSet rs = null;

    @Override
    public boolean add(EmployeeApplication employeeApplication) {
        try {
            this.conn = DAOUtilities.getConnection();
            String sql = "INSERT INTO employeeApplication VALUES (default,null,?,?,?,?,?,?)";
            this.stmnt = this.conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);

            //this.stmnt.setInt(1, employeeApplication.getManagerID());
            this.stmnt.setString(1, employeeApplication.getStatus());
            this.stmnt.setString(2, employeeApplication.getComment());
            this.stmnt.setString(3, employeeApplication.getfName());
            this.stmnt.setString(4, employeeApplication.getlName());
            this.stmnt.setString(5, employeeApplication.getPhoneNo());
            this.stmnt.setString(6, employeeApplication.getEmail());

            if (this.stmnt.executeUpdate() != 0) {
                rs = stmnt.getGeneratedKeys();

                if (rs.next()) {
                    employeeApplication.setApplicationID(rs.getInt(1));
                }

                return true;
            }
            return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            this.closeResources();
        }
    }

    @Override
    public boolean update(EmployeeApplication employeeApplication) {
        try {
            this.conn = DAOUtilities.getConnection();
            String sql = "UPDATE employeeApplication SET managerID=?, status=?, comment=? WHERE applicationID=?";
            this.stmnt = this.conn.prepareStatement(sql);

            this.stmnt.setInt(1, employeeApplication.getManagerID());
            this.stmnt.setString(2, employeeApplication.getStatus());
            this.stmnt.setString(3, employeeApplication.getComment());
            this.stmnt.setInt(4, employeeApplication.getApplicationID());

            if (this.stmnt.executeUpdate() != 0) {
                return true;
            }
            return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            this.closeResources();
        }

    }

    @Override
    public EmployeeApplication get(int applicationID){
        EmployeeApplication ea = null;

        try {
            this.conn = DAOUtilities.getConnection();
            String sql = "SELECT * FROM employeeApplication WHERE applicationID=?";
            this.stmnt = this.conn.prepareStatement(sql);

            this.stmnt.setInt(1, applicationID);

            rs = this.stmnt.executeQuery();

            while(rs.next()){
                ea = new EmployeeApplication(rs.getInt("applicationID"),rs.getInt("managerID"),rs.getString("status"),rs.getString("comment"),rs.getString("fname"),rs.getString("lname"),rs.getString("phonenumber"),rs.getString("email"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            this.closeResources();
        }

        return ea;
    }

    @Override
    public List<EmployeeApplication> getByStatus(String status) {
        List<EmployeeApplication> employeeApplications = new ArrayList<EmployeeApplication>();
        
        try {
            this.conn = DAOUtilities.getConnection();
            String sql = "SELECT * FROM employeeApplication WHERE status=?";
            this.stmnt = this.conn.prepareStatement(sql);

            this.stmnt.setString(1, status);

            rs = this.stmnt.executeQuery();

            while(rs.next()){
                employeeApplications.add(new EmployeeApplication(rs.getInt("applicationID"),rs.getInt("managerID"),rs.getString("status"),rs.getString("comment"),rs.getString("fname"),rs.getString("lname"),rs.getString("phonenumber"),rs.getString("email")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            this.closeResources();
        }

        return employeeApplications;
    }

    /**
     * Closing all resource, connection and statements
     */
    private void closeResources() {

        try {
            if (rs != null)
                rs.close();
        } catch (SQLException e) {
            System.out.println("Could not close the result set!");
            e.printStackTrace();
        }

        try {
            if (stmnt != null)
                stmnt.close();
        } catch (SQLException e) {
            System.out.println("Could not close statement!");
            e.printStackTrace();
        }

        try {
            if (conn != null)
                conn.close();
        } catch (SQLException e) {
            System.out.println("Could not close the connection!");
            e.printStackTrace();
        }
    }

}
