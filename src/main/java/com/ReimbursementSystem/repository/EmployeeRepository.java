package com.ReimbursementSystem.repository;

import java.util.List;

import com.ReimbursementSystem.model.Employee;
import com.fasterxml.jackson.databind.node.ObjectNode;

public interface EmployeeRepository {
    public Employee getEmployeeByID(int id);
    public boolean addEmployee(Employee employee);
    public boolean updateEmployee(Employee employee);
    public Employee getEmployeeByEmail(String email); 
    public boolean deleteEmployee(Employee employee);
    public boolean deleteEmployeeByID(int id);
    public List<Employee> getAllEmployee();
    public List<Employee> getAllEmployeeByManager(int managerID);
    public List<Employee> getAllManagersByJoin();
    public List<Employee> getAllManagers();
    public boolean updateManagerStatus(Employee employee);
    public List<ObjectNode> getAllEmployeesByJoin();
}
