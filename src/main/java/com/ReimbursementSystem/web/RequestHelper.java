package com.ReimbursementSystem.web;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ReimbursementSystem.model.Reciept;
import com.ReimbursementSystem.service.EmployeeApplicationService;
import com.ReimbursementSystem.service.EmployeeService;
import com.ReimbursementSystem.service.RecieptService;
import com.ReimbursementSystem.service.ReimbursementService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class RequestHelper {
	public static Object processGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		final String URL = request.getRequestURI();
		// debug line
		System.out.println(URL);

		final String RESOURCE = URL.replace("/ReimbursementSystem/myapi", "");
		System.out.println(RESOURCE);

		HttpSession session = request.getSession(false);

		switch (RESOURCE) {
			case "/getuname":
				String test = new EmployeeService().getEmployeeName((Integer) session.getAttribute("employeeID"));
				System.out.println(test);
				return test;

			case "/getEmployeeName":
				return new EmployeeService().getEmployeeName(request);

			case "/logout":
				if (session != null) {
					session.invalidate();
					System.out.println("Invalidated session");
				}
				break;

			case "/ePendingRequests":
				request.setAttribute("status", "pending");
				//return new ReimbursementService().getByStatus(request);
				return new ReimbursementService().getByEmployeeIdWithReceipt(request);

			case "/eResolvedRequests":
				request.setAttribute("status", "resolved");
				return new ReimbursementService().getByEmployeeIdWithReceipt(request);

			case "/eInfo":
				return new EmployeeService().getEmployeeByID(request);

			case "/approveRequest":
				return new ReimbursementService().approveRequest(request);

			case "/denyRequest":
				return new ReimbursementService().denyRequest(request);

			case "/getReceiptByReimbursementID":
				return new RecieptService().getRecieptByReimbursementID(request);

			case "/mPendingRequests":
				return new ReimbursementService().getPendingRequestsByManagerIdWithReceipt(request);

			case "/mReciepts":
				return new RecieptService().getAll(request);

			case "/mAllResolvedRequests":
				request.setAttribute("status", "pending");
				return new ReimbursementService().getByNotStatusWithReceipt(request);

			case "/mAllEmployees":
				return new EmployeeService().getAllEmployeesByJoin();

			case "/mGetMyEmployees":
				return new EmployeeService().getAllEmployeeByManager((int) session.getAttribute("employeeID"));

			case "/mGetAllRequestsByEmployee":
				request.setAttribute("status", "none");
				return new ReimbursementService().getByEmployeeIdWithReceipt(request);

			case "/promote":
				return new EmployeeService().promoteEmployee(request);

			case "/registerEmployee":
				return new EmployeeService().registerEmployee(request);

			case "/getPendingApplications":
				return new EmployeeApplicationService().getByStatus("pending");

			case "/rejectApplication":
				return new EmployeeApplicationService().rejectApplication(request);

			case "/test":
				Reciept r = new RecieptService().getRecieptByReimbursementID(7).get(0);
				
				/**/
				ObjectMapper mapper = new ObjectMapper();
				ObjectNode reciept = mapper.createObjectNode();
				reciept.put("reimbursementID",r.getReimbursementID());
				reciept.put("receiptName",r.getRecieptName());
				reciept.put("fileType",r.getFileType());
				reciept.put("content",r.getContent());
				/**/

				return reciept;
					

			default:
				return "No such endpoint or resource";
		}

		return null;

	}

	public static void processPost(HttpServletRequest request, HttpServletResponse response) {
		final String URL = request.getRequestURI();
		System.out.println(URL);

		final String RESOURCE = URL.replace("/ReimbursementSystem/myapi", "");
		System.out.println(RESOURCE);

		switch (RESOURCE) {
			case "/submitRequest":
				if (new ReimbursementService().add(request) == false)
					response.setStatus(499);
				break;

			case "/updateInfo":
				new EmployeeService().updateEmployee(request);
				break;
				
			default:
				break;
		}

	}
}
