package com.ReimbursementSystem.web;

import java.io.IOException;

import javax.lang.model.util.ElementScanner6;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ReimbursementSystem.service.AuthTableService;
import com.ReimbursementSystem.service.EmployeeService;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private AuthTableService authTableService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        authTableService = new AuthTableService();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int employeeID = this.authTableService.getID(request.getParameter("username"), request.getParameter("password"));
		System.out.println(employeeID);
		if(employeeID != 0) {
			// if login is valid then get session and set session employee id to the corresponding employee id
			HttpSession session = request.getSession();
			session.setAttribute("employeeID", employeeID);
			//session.setAttribute("employeeID", employeeID);

			//response.setHeader("employeeID", String.valueOf(employeeID));
			//response.setHeader("Content-Length", String.valueOf(20));
			//response.addIntHeader("employeeID", employeeID);
/*
			Cookie c = new Cookie("employeeID", String.valueOf(employeeID));
			c.setMaxAge(30);

			response.addCookie(c);
/**/
			if(new EmployeeService().isManager(employeeID)){
				session.setAttribute("isManager", true);
				response.sendRedirect("./manager/manager.html");
			}
			else {
				session.setAttribute("isManager", false);
				response.sendRedirect("./pages/employee.html");
			}

			/*
			RequestDispatcher dispatcher = request.getRequestDispatcher("./pages/employee.html");
			dispatcher.forward(request, response);
			/**/

		} else {
			response.sendRedirect("index.html");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
