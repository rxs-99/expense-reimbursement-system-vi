package com.ReimbursementSystem.utilities;

import org.apache.commons.validator.routines.EmailValidator;

public class inputValidation {
    /**
	 * This method returns true if there is a space in the input
	 * @param input
	 * @return
	 */
	public static boolean containSpaces(String input) {
		
		if (input.contains(" "))
			return true;
		
		return false;
    }
    
    public static boolean isInt(String input){
        try{
            int value = Integer.valueOf(input);
        } catch (Exception e){
            System.out.println("Not an integer!");
            return false;
        }

        return true;
    }

    public static boolean isDouble(String input){
        try{
            double value = Double.valueOf(input);
        } catch (Exception e){
            System.out.println("Not a double!");
            return false;
        }

        return true;
    }

    public static boolean isOnlyAlphabets(String input){
        char[] temp = input.toLowerCase().toCharArray();

        for(int i = 0; i < temp.length; i++){
            char ch = temp[i];
            if(ch < 'a' || ch > 'z')
                return false;
        }

        return true; 
    }

    public static boolean isValidEmail(String input){
        return EmailValidator.getInstance().isValid(input);
    }

    public static boolean isBoolean(String input){
        
        if(input != null){
            if(Boolean.valueOf(input))
                return true;
            if(input.toLowerCase().contains("false") && input.length() == 5)
                return true;
        }

        return false;
    }
}
