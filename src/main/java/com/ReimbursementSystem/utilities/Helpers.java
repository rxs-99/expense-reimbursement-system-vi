package com.ReimbursementSystem.utilities;

import java.util.Random;

import com.ReimbursementSystem.model.Employee;

public class Helpers {
    public String generateUsername(Employee employee) {
        String uName = "";
        uName += employee.getfName().substring(0, 1).toLowerCase();
        uName += employee.getlName().substring(0, 1).toLowerCase();
        uName += employee.getEmployeeID();
        uName += this.randomNumString();
        return uName;
    }

    // not my actual code. Taken from internet
    public String generatePassword(int length) {
        char[] password = new char[length];

        String capitalCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
        String specialCharacters = "!@#$";
        String numbers = "1234567890";
        String combinedChars = capitalCaseLetters + lowerCaseLetters + specialCharacters + numbers;
        Random random = new Random();

        password[0] = lowerCaseLetters.charAt(random.nextInt(lowerCaseLetters.length()));
        password[1] = capitalCaseLetters.charAt(random.nextInt(capitalCaseLetters.length()));
        password[2] = specialCharacters.charAt(random.nextInt(specialCharacters.length()));
        password[3] = numbers.charAt(random.nextInt(numbers.length()));

        for (int i = 4; i < length; i++) {
            password[i] = combinedChars.charAt(random.nextInt(combinedChars.length()));
        }
        return String.valueOf(password);
    }

    public String randomNumString() {
        /*
        Random random = new Random();
        int num = random.nextInt(999999);
        */
        return String.format("%06d", new Random().nextInt(999999));
    }
}
