package com.ReimbursementSystem.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.ReimbursementSystem.repository.AuthTableRepository;
import com.ReimbursementSystem.repository.AuthTableRepositoryImpl;
import com.ReimbursementSystem.repository.DeniedReimbursementRepository;
import com.ReimbursementSystem.repository.DeniedReimbursementRepositoryImpl;
import com.ReimbursementSystem.repository.EmployeeApplicationRepository;
import com.ReimbursementSystem.repository.EmployeeApplicationRepositoryImpl;
import com.ReimbursementSystem.repository.EmployeeRepository;
import com.ReimbursementSystem.repository.EmployeeRepositoryImpl;
import com.ReimbursementSystem.repository.RecieptRepository;
import com.ReimbursementSystem.repository.RecieptRepositoryImpl;
import com.ReimbursementSystem.repository.ReimbursementRepository;
import com.ReimbursementSystem.repository.ReimbursementRepositoryImpl;


/**
 * Class used to retrieve DAO Implementations. Serves as a factory. Also manages a single instance of the database connection.
 */

public class DAOUtilities {
	private static final String CONNECTION_USERNAME = GetAuth.property.getProperty("user");
	private static final String CONNECTION_PASSWORD = GetAuth.property.getProperty("password");
	private static final String URL = GetAuth.property.getProperty("url");
	
	private static Properties props = GetAuth._property;

	private static Connection connection;
	
	public static synchronized Connection getConnection() throws SQLException {

		if (connection == null) {
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				System.out.println("Could not register driver!");
				e.printStackTrace();
			}
			
			connection = DriverManager.getConnection(URL,props);
		}
		
		if(connection.isClosed()) {
			//System.out.println("Opening new connection...");
			connection = DriverManager.getConnection(URL,props);
		}

		return connection;
	}

	public static EmployeeRepository getEmployeeRepository(){
		return new EmployeeRepositoryImpl();
	}

	public static AuthTableRepository getAuthTableRepository(){
		return new AuthTableRepositoryImpl();
	}

	public static EmployeeApplicationRepository getEmployeeApplicationRepository(){
		return new EmployeeApplicationRepositoryImpl();
	}

	public static ReimbursementRepository getReimbursementRepository(){
		return new ReimbursementRepositoryImpl();
	}

	public static DeniedReimbursementRepository getDeniedReimbursementRepository(){
		return new DeniedReimbursementRepositoryImpl();
	}

	public static RecieptRepository getRecieptRepository(){
		return new RecieptRepositoryImpl();
	}
	
}
