package com.ReimbursementSystem.utilities;

import com.ReimbursementSystem.model.Employee;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;

public class testHelpers {
    @InjectMocks
    private static Helpers h;

    // spy
    private static Helpers _h;

    @BeforeClass
    public static void beforeClass(){
        h = new Helpers();
        _h = Mockito.spy(h);
    }

    @Test
    public void testGenerateUsername(){
        Mockito.when(_h.randomNumString()).thenReturn("111111");
        //Mockito.doReturn("111111").when(_h).randomNumString();
        
        Assert.assertEquals("fl2111111", _h.generateUsername(new Employee(2,"fName","lNmae","ersdf","asdf")));
    }
}
