package com.ReimbursementSystem.utilities;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ReimbursementSystem.exceptions.alreadySetUpDatabaseAuthInfoException;

public class DAOUtilitiesTest {
	private static Connection connection = null;
	
	@BeforeClass
	public static void setUp() {
		try {
			GetAuth.setProperties();
		} catch (alreadySetUpDatabaseAuthInfoException e1) {
			e1.printStackTrace();
		}
	}
	
	@Test
	public void testConnection() {
		try {
			connection = DAOUtilities.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Assert.assertNotNull(connection);
	}
	
	@AfterClass
	public static void closeResources() {
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			System.out.println("Could not close the connection!");
			e.printStackTrace();
		}
		
		GetAuth.setDefault();
	}
}
