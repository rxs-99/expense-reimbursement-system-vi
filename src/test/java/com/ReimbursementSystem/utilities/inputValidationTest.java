package com.ReimbursementSystem.utilities;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class inputValidationTest {
    @BeforeClass
    public static void beforeClass(){

    }

    @Test
    public void testContainSpaces(){
        Assert.assertTrue(inputValidation.containSpaces("test space"));
        Assert.assertFalse(inputValidation.containSpaces("Testnospace"));
    }

    @Test
    public void testIsInt(){
        Assert.assertTrue(inputValidation.isInt("23"));
        Assert.assertFalse(inputValidation.isInt("23.3"));
        Assert.assertFalse(inputValidation.isInt("not int"));
        Assert.assertFalse(inputValidation.isInt(""));
        Assert.assertFalse(inputValidation.isInt(null));
    }

    @Test
    public void testIsDouble(){
        Assert.assertTrue(inputValidation.isDouble("23"));
        Assert.assertTrue(inputValidation.isDouble("23.26"));
        Assert.assertFalse(inputValidation.isDouble("not int"));
        Assert.assertFalse(inputValidation.isDouble(""));
        Assert.assertFalse(inputValidation.isDouble(null));
    }

    @Test
    public void testIsOnlyAlphabets(){
        Assert.assertTrue(inputValidation.isOnlyAlphabets("abcde"));
        Assert.assertTrue(inputValidation.isOnlyAlphabets("AserSDF"));
        Assert.assertFalse(inputValidation.isOnlyAlphabets("123554"));
        Assert.assertFalse(inputValidation.isOnlyAlphabets("$@%$^"));
        Assert.assertFalse(inputValidation.isOnlyAlphabets("feFDFJ!*df"));
        Assert.assertFalse(inputValidation.isOnlyAlphabets("adsf2w33e"));
    }

    @Test
    public void testIsValidEmail(){
        Assert.assertTrue(inputValidation.isValidEmail("rabinson.shrestha@revature.net"));
        Assert.assertFalse(inputValidation.isValidEmail("lahdfewi"));
        Assert.assertFalse(inputValidation.isValidEmail("345345"));
        Assert.assertFalse(inputValidation.isValidEmail("sdfasd89976f"));
        Assert.assertFalse(inputValidation.isValidEmail("@sdf.com"));
        Assert.assertFalse(inputValidation.isValidEmail("@@.@"));
        Assert.assertFalse(inputValidation.isValidEmail("asdf@asele.fge"));
        Assert.assertTrue(inputValidation.isValidEmail("a@b.com"));
    }

    @Test
    public void testIsBoolean(){
        Assert.assertTrue(inputValidation.isBoolean("true"));
        Assert.assertTrue(inputValidation.isBoolean("false"));
        Assert.assertFalse(inputValidation.isBoolean("input"));
        Assert.assertFalse(inputValidation.isBoolean("1243"));
        Assert.assertFalse(inputValidation.isBoolean("atruea"));
        Assert.assertFalse(inputValidation.isBoolean("falsea"));
    }

    @AfterClass
    public static void afterClass(){

    }
}
