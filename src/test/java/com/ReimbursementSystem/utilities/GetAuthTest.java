package com.ReimbursementSystem.utilities;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import com.ReimbursementSystem.exceptions.alreadySetUpDatabaseAuthInfoException;

public class GetAuthTest {
	
	@Test
	public void testGetAuth() {
		
		Assert.assertNull(GetAuth.property);
		
		try {
			GetAuth.setProperties();
			Assert.assertNotNull(GetAuth.property);
		} catch (alreadySetUpDatabaseAuthInfoException e) {
			Assert.fail("First execution of method shall not throw exception!");
		}
		
		try {
			GetAuth.setProperties();
			Assert.fail("Second execution of method shall throw an exception!");
		} catch (alreadySetUpDatabaseAuthInfoException e) {

		}
	}
	
	@AfterClass
	public static void afterClass() {
		GetAuth.setDefault();
	}
}
