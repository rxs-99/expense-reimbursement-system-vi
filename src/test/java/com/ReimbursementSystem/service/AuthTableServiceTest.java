package com.ReimbursementSystem.service;

import com.ReimbursementSystem.model.AuthTable;
import com.ReimbursementSystem.repository.AuthTableRepository;
import com.ReimbursementSystem.repository.AuthTableRepositoryImpl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class AuthTableServiceTest {
    @InjectMocks
    private static AuthTableService ats;

    @Mock
    private AuthTableRepository atr;

    @BeforeClass
    public static void beforeClass(){
        ats = new AuthTableService(new AuthTableRepositoryImpl());
    }

    @Before
    public void before(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testAdd(){
        AuthTable auth = new AuthTable();
        Mockito.when(this.atr.add(auth)).thenReturn(true);

        Assert.assertTrue(ats.add(auth));
    }

    @Test
    public void testUpdate(){
        AuthTable auth = new AuthTable();
        Mockito.when(this.atr.update(auth)).thenReturn(false);

        Assert.assertFalse(ats.update(auth));
    }

    @Test
    public void testIsDuplicateUsername(){
        Mockito.when(this.atr.isDuplicateUsername("rdfse")).thenReturn(true);

        Assert.assertTrue(ats.isDuplicateUsername("rdfse"));
        Assert.assertFalse(ats.isDuplicateUsername("user"));

    }

    @Test
    public void testGetID(){
        Mockito.when(this.atr.getID("test", "pass")).thenReturn(13);

        Assert.assertEquals(13, ats.getID("test", "pass"));
    }

    @Test
    public void testIsValidLogin(){
        Mockito.when(ats.getID("test", "pass")).thenReturn(2);

        Assert.assertTrue(ats.isValidLogin("test", "pass"));
        Assert.assertFalse(ats.isValidLogin("username", "password"));
    }
}
