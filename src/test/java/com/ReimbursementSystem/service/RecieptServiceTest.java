package com.ReimbursementSystem.service;

import java.util.Arrays;

import com.ReimbursementSystem.model.Reciept;
import com.ReimbursementSystem.repository.RecieptRepository;
import com.ReimbursementSystem.repository.RecieptRepositoryImpl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class RecieptServiceTest {
    @InjectMocks
    private static RecieptService rs;

    @Mock
    private RecieptRepository rr;

    @BeforeClass
    public static void beforeClass(){
        rs = new RecieptService(new RecieptRepositoryImpl());
    }

    @Before
    public void before(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testAdd(){
        Reciept r = new Reciept();
        Mockito.when(this.rr.add(r)).thenReturn(true);

        Assert.assertTrue(rs.add(r));
    }

    @Test
    public void testAddUsingStream(){
        Reciept r = new Reciept();
        Mockito.when(this.rr.addUsingStream(r)).thenReturn(true);

        Assert.assertTrue(rs.addUsingStream(r));
    }

    @Test
    public void testGetRecieptByReimbursementID(){
        Mockito.when(this.rr.getRecieptByReimbursementID(36)).thenReturn(Arrays.asList(new Reciept(), new Reciept()));
        
        Assert.assertEquals(2, rs.getRecieptByReimbursementID(36).size());
        Assert.assertEquals(0, rs.getRecieptByReimbursementID(6).size());
    }

    @Test
    public void testGetAll(){
        Mockito.when(this.rr.getAll()).thenReturn(Arrays.asList(new Reciept(), new Reciept(), new Reciept(), new Reciept()));
        
        Assert.assertEquals(4, rs.getAll().size());
    }
}
