package com.ReimbursementSystem.service;

import java.util.Arrays;

import com.ReimbursementSystem.model.EmployeeApplication;
import com.ReimbursementSystem.repository.EmployeeApplicationRepository;
import com.ReimbursementSystem.repository.EmployeeApplicationRepositoryImpl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class EmployeeApplicationServiceTest {
    @InjectMocks
    private static EmployeeApplicationService eas;

    @Mock
    private EmployeeApplicationRepository ear;

    @BeforeClass
    public static void beforeClass(){
        eas = new EmployeeApplicationService(new EmployeeApplicationRepositoryImpl());
    }

    @Before
    public void before(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testAdd(){
        EmployeeApplication ea = new EmployeeApplication();
        Mockito.when(this.ear.add(ea)).thenReturn(true);

        Assert.assertFalse(eas.add(ea));
    }

    @Test
    public void testUpdate(){
        EmployeeApplication ea = new EmployeeApplication();
        Mockito.when(this.ear.update(ea)).thenReturn(true);

        Assert.assertTrue(eas.update(ea));
    }

    @Test
    public void testGet(){
        Mockito.when(this.ear.get(2)).thenReturn(new EmployeeApplication(2,1,null,null));

        Assert.assertEquals(2, eas.get(2).getApplicationID());
    }

    @Test
    public void testGetByStatus(){
        Mockito.when(this.ear.getByStatus("pending")).thenReturn(Arrays.asList());

        Assert.assertEquals(0,eas.getByStatus("pending").size());
    }
}
