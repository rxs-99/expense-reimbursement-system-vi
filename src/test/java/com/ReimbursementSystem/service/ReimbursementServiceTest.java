package com.ReimbursementSystem.service;

import java.util.Arrays;

import com.ReimbursementSystem.model.Reimbursement;
import com.ReimbursementSystem.repository.ReimbursementRepository;
import com.ReimbursementSystem.repository.ReimbursementRepositoryImpl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class ReimbursementServiceTest {
    @InjectMocks
    private static ReimbursementService rs;

    @Mock
    private ReimbursementRepository rr;

    @BeforeClass
    public static void beforeClass(){
        rs = new ReimbursementService(new ReimbursementRepositoryImpl());
    }

    @Before
    public void before(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testAdd(){
        Reimbursement r = new Reimbursement();
        Mockito.when(this.rr.add(r)).thenReturn(false);

        Assert.assertFalse(rs.add(r));

    }

    @Test
    public void testUpdate(){
        Reimbursement r = new Reimbursement();
        Mockito.when(this.rr.update(r)).thenReturn(true);

        Assert.assertTrue(rs.update(r));
    }

    @Test
    public void testGetByEmployeeID(){
        Mockito.when(this.rr.getByEmployeeID(21)).thenReturn(Arrays.asList(new Reimbursement(), new Reimbursement()));

        Assert.assertEquals(2, rs.getByEmployeeID(21).size());
    }

    @Test
    public void testGetAll(){
        Mockito.when(this.rr.getAll()).thenReturn(Arrays.asList(new Reimbursement(), new Reimbursement()));

        Assert.assertEquals(2, rs.getAll().size());
    }

    @Test
    public void testGetByStatus(){
        Mockito.when(this.rr.getByStatus("testing")).thenReturn(Arrays.asList(new Reimbursement(), new Reimbursement()));

        Assert.assertEquals(2, rs.getByStatus("testing").size());
        Assert.assertEquals(0, rs.getByStatus("te").size());
    }

    @Test
    public void testGetByNotStatus(){
        Mockito.when(this.rr.getByNotStatus("testing")).thenReturn(Arrays.asList(new Reimbursement(), new Reimbursement()));

        Assert.assertEquals(2, rs.getByNotStatus("testing").size());
        Assert.assertEquals(0, rs.getByNotStatus("te").size());
    }

    // TODO
    // add remainning tests later
}
