package com.ReimbursementSystem.service;

import java.util.Arrays;

import com.ReimbursementSystem.model.Employee;
import com.ReimbursementSystem.repository.EmployeeRepository;
import com.ReimbursementSystem.repository.EmployeeRepositoryImpl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class EmployeeServiceTest {
    @InjectMocks
    private static EmployeeService es;

    @Mock
    private EmployeeRepository er;

    @BeforeClass
    public static void beforeClass(){
        es = new EmployeeService(new EmployeeRepositoryImpl());
    }

    @Before
	public void before() {
		MockitoAnnotations.openMocks(this);
	}

    @Test
    public void testGetEmployeeByID(){
        Mockito.when(this.er.getEmployeeByID(1)).thenReturn(new Employee(1,"fname","lname","phone","email"));

        Employee e = es.getEmployeeByID(1);

        Assert.assertEquals(1, e.getEmployeeID());
        Assert.assertFalse(e.isManager());
        Assert.assertFalse(e.isFirstLogin());
        Assert.assertEquals("phone", e.getPhoneNo());

    }

    @Test
    public void testGetEmployeeByName(){
        Employee e = new Employee();
        e.setfName("hello");

        Mockito.when(es.getEmployeeByID(2)).thenReturn(e);

        Assert.assertEquals("hello", es.getEmployeeName(2));
    }

    @Test
    public void testAddEmployee(){
        Employee e = new Employee();
        Mockito.when(this.er.addEmployee(e)).thenReturn(true);
        Assert.assertEquals(true, es.addEmployee(e));
    }

    @Test
    public void testUpdateEmployee(){
        Employee e = new Employee();
        Mockito.when(this.er.updateEmployee(e)).thenReturn(true);
        Assert.assertEquals(true, es.updateEmployee(e));
    }

    @Test
    public void testGetEmployeeByEmail(){
        Mockito.when(this.er.getEmployeeByEmail("a@a.a")).thenReturn(new Employee(12,"fname","lname","phone","email"));

        Assert.assertEquals(12, es.getEmployeeByEmail("a@a.a").getEmployeeID());
    }

    @Test
    public void testDeleteEmployee(){
        Employee e = new Employee();
        Mockito.when(this.er.deleteEmployee(e)).thenReturn(true);
        Assert.assertEquals(true, es.deleteEmployee(e));
    }

    @Test
    public void testDeleteEmployeeByID(){
        Mockito.when(this.er.deleteEmployeeByID(14)).thenReturn(true);
        Assert.assertEquals(true, es.deleteEmployeeByID(14));
    }

    @Test
    public void testGetAllEmployee(){
        Mockito.when(this.er.getAllEmployee()).thenReturn(Arrays.asList(new Employee(), new Employee(), new Employee()));

        Assert.assertEquals(3, es.getAllEmployee().size());
        Assert.assertNull(es.getAllEmployee().get(2).getfName());
    }

    @Test
    public void testGetAllEmployeeByManager(){
        Mockito.when(this.er.getAllEmployeeByManager(2)).thenReturn(Arrays.asList(new Employee(), new Employee()));

        Assert.assertEquals(2, es.getAllEmployeeByManager(2).size());
    }

    @Test
    public void testGetAllManagerByJoin(){
        Mockito.when(this.er.getAllManagersByJoin()).thenReturn(Arrays.asList(new Employee()));

        Assert.assertEquals(1, es.getAllManagerByJoin().size());
    }

    @Test
    public void testGetAllManagers(){
        Mockito.when(this.er.getAllManagers()).thenReturn(Arrays.asList(new Employee(), new Employee()));

        Assert.assertEquals(2, es.getAllManagers().size());
    }

    @Test
    public void testIsManager(){
        Mockito.when(es.getAllManagers()).thenReturn(Arrays.asList(new Employee(1,true), new Employee(3,true)));

        Assert.assertTrue(es.isManager(3));
        Assert.assertFalse(es.isManager(2));
    }
}
