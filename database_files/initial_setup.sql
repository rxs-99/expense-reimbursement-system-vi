-- Drop tables from project 1 if it exists

drop table if exists reciept;
drop table if exists deniedReimbursement;
drop table if exists reimbursement;
drop table if exists authTable;
drop table if exists employeeApplication;
drop table if exists employee;

create table if not exists employee(
	employeeID serial unique,
	fName varchar (50) not null,
	lName varchar (50) not null,
	phoneNumber varchar (20) unique not null,
	email varchar (100) unique not null,
	firstLogin boolean not null,
	availableFunds numeric(10,2) not null check (availableFunds > 0),
	employeeSince date not null,
	managerID int null,
	isManager boolean not null,
	primary key (employeeID),
	foreign key (managerID) references employee(employeeID)
);


create table if not exists employeeApplication(
	applicationID serial unique,
	managerID int null,
	status varchar (10) not null,
	comment varchar (100) null,
	fName varchar (50) not null,
	lName varchar (50) not null,
	phoneNumber varchar (20) unique not null,
	email varchar (100) unique not null,
	primary key (applicationID),
	foreign key (managerID) references employee(employeeID)
);

create table if not exists authTable(
	employeeID int unique not null,
	username varchar (20) unique not null,
	password varchar (64) not null,
	primary key (employeeID),
	foreign key (employeeID) references employee(employeeID)
);

create table if not exists reimbursement(
	reimbID serial unique,
	employeeID int not null,
	type varchar (25) not null,
	reciept bytea,
	status varchar (15) not null,
	createdOn date not null,
	priority varchar (10) not null,		-- optional
	eventDate date not null,			-- optional
	amount numeric(10,2) not null,
	managerID int null,
	primary key (reimbID),
	foreign key (employeeID) references employee(employeeID),
	foreign key (managerID) references employee(employeeID)
);

create table if not exists deniedReimbursement(
	reimbID int unique not null,
	deniedReason varchar (100) not null,
	primary key (reimbID),
	foreign key (reimbID) references reimbursement(reimbID)
);

create table if not exists reciept(
	recieptID serial unique,
	reimbursementID int not null,
	recieptName varchar (100) not null,
	fileType varchar (100) not null,
	content bytea not null,
	primary key (recieptID),
	foreign key (reimbursementID) references reimbursement(reimbID)
);

insert into employee values (default,'James','Hogan','6154129364','jamesshogan@jourrapide.com',false,1000.00,'2020-10-10',null);
insert into authtable values (1,'admin1','password');
insert into employee values (default,'Dayna','Brown','8068568404','daynajbrown@jourrapide.com',false,1000.00,'2020-10-10',1);
insert into authtable values(2,'admin2','password');
update employee set managerID=2 where employeeID=1;
select * from employee;
select * from authtable;





/****************************************************
 ************** REVATURE ACTIVITY STUFFS ************
 ****************************************************/

create sequence test start with 6 increment by 3;

delete table if exists user_role;
delete table if exists app_user;
delete table if exists category;
delete table if exists flashcard;
delete table if exists study_set;
delete table if exists study_set_card;

create table user_role(
	role_id serial primary key,
	name varchar unique not null	
);

create table app_user(
	user_id serial primary key,
	username varchar unique not null,
	password varchar not null,
	first_name varchar not null,
	last_name varchar not null,
	role_id int,
	foreign key (role_id) references user_role(role_id)
);

create table category(
	category_id serial primary key,
	name varchar unique not null
);

create table flashcard(
	flashcard_id serial primary key,
	question varchar unique not null,
	answer varchar not null,
	category_id int,
	foreign key (category_id) references category(category_id)
);

create table study_set(
	study_set_id serial primary key,
	name varchar unique not null,
	owner_id int,
	foreign key (owner_id) references app_user(user_id)
);

create table study_set_card(
	study_set_id int,
	flashcard_id int,
	primary key (study_set_id,flashcard_id),
	foreign key (study_set_id) references study_set(study_set_id),
	foreign key (flashcard_id) references flashcard(flashcard_id)
);



select ABS(12);




